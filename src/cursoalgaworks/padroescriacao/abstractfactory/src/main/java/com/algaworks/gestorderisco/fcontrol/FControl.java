package cursoalgaworks.padroescriacao.abstractfactory.src.main.java.com.algaworks.gestorderisco.fcontrol;

import java.math.BigDecimal;

import cursoalgaworks.padroescriacao.abstractfactory.src.main.java.com.algaworks.gestorderisco.AlertaDeRiscoException;
import cursoalgaworks.padroescriacao.abstractfactory.src.main.java.com.algaworks.gestorderisco.GestorDeRisco;

public class FControl implements GestorDeRisco {

    @Override
    public void avaliarRisco(String cartao, BigDecimal valor) throws AlertaDeRiscoException {
        if (cartao.startsWith("7777")) {
            throw new AlertaDeRiscoException("Cartão suspeito.");
        }
    }

}

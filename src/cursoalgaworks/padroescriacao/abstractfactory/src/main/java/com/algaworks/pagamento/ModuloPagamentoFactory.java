package cursoalgaworks.padroescriacao.abstractfactory.src.main.java.com.algaworks.pagamento;

import cursoalgaworks.padroescriacao.abstractfactory.src.main.java.com.algaworks.gestorderisco.GestorDeRisco;
import cursoalgaworks.padroescriacao.abstractfactory.src.main.java.com.algaworks.operadora.Operadora;

public interface ModuloPagamentoFactory {

    public Operadora criarOperadora();

    public GestorDeRisco criarGestorDeRisco();

}

package cursoalgaworks.padroescriacao.abstractfactory.src.main.java.com.algaworks.pagamento.paypal;

import cursoalgaworks.padroescriacao.abstractfactory.src.main.java.com.algaworks.gestorderisco.GestorDeRisco;
import cursoalgaworks.padroescriacao.abstractfactory.src.main.java.com.algaworks.gestorderisco.clearsale.ClearSale;
import cursoalgaworks.padroescriacao.abstractfactory.src.main.java.com.algaworks.operadora.Operadora;
import cursoalgaworks.padroescriacao.abstractfactory.src.main.java.com.algaworks.operadora.redecard.Redecard;
import cursoalgaworks.padroescriacao.abstractfactory.src.main.java.com.algaworks.pagamento.ModuloPagamentoFactory;

public class PayPalModuloPagamentoFactory implements ModuloPagamentoFactory {

    @Override
    public Operadora criarOperadora() {
        return new Redecard();
    }

    @Override
    public GestorDeRisco criarGestorDeRisco() {
        return new ClearSale();
    }

}

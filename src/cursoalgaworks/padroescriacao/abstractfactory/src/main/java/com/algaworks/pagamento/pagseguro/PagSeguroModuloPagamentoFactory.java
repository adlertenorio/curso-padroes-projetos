package cursoalgaworks.padroescriacao.abstractfactory.src.main.java.com.algaworks.pagamento.pagseguro;

import cursoalgaworks.padroescriacao.abstractfactory.src.main.java.com.algaworks.gestorderisco.GestorDeRisco;
import cursoalgaworks.padroescriacao.abstractfactory.src.main.java.com.algaworks.gestorderisco.fcontrol.FControl;
import cursoalgaworks.padroescriacao.abstractfactory.src.main.java.com.algaworks.operadora.Operadora;
import cursoalgaworks.padroescriacao.abstractfactory.src.main.java.com.algaworks.operadora.cielo.Cielo;
import cursoalgaworks.padroescriacao.abstractfactory.src.main.java.com.algaworks.pagamento.ModuloPagamentoFactory;

public class PagSeguroModuloPagamentoFactory implements ModuloPagamentoFactory {

    @Override
    public Operadora criarOperadora() {
        return new Cielo();
    }

    @Override
    public GestorDeRisco criarGestorDeRisco() {
        return new FControl();
    }

}

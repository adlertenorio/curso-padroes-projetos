package cursoalgaworks.padroescriacao.builder.src.main.java.com.algaworks.model.builder;

import cursoalgaworks.padroescriacao.builder.src.main.java.com.algaworks.model.PedidoVenda;

public class PedidoVendaBuilderValido {

    private PedidoVenda instancia;

    public PedidoVendaBuilderValido(PedidoVenda instancia) {
        this.instancia = instancia;
    }

    public PedidoVenda construir() {
        return this.instancia;
    }

}

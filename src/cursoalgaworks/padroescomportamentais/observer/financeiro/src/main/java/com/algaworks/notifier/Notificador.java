package cursoalgaworks.padroescomportamentais.observer.financeiro.src.main.java.com.algaworks.notifier;

import java.util.List;

import cursoalgaworks.padroescomportamentais.observer.financeiro.src.main.java.com.algaworks.listeners.Listener;
import cursoalgaworks.padroescomportamentais.observer.financeiro.src.main.java.com.algaworks.model.Lancamento;

public interface Notificador {

    public void registrarListener(Listener listener);

    public void removerListener(Listener listener);

    public void notificarListeners();

    public void novosLancamentosVencidos(List<Lancamento> lancamentosVencidos);

    public List<Lancamento> getLancamentosVencidos();

}

package cursoalgaworks.padroescomportamentais.observer.financeiro.src.main.java.com.algaworks.job;

import java.util.List;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import cursoalgaworks.padroescomportamentais.observer.financeiro.src.main.java.com.algaworks.model.Lancamento;
import cursoalgaworks.padroescomportamentais.observer.financeiro.src.main.java.com.algaworks.notifier.Notificador;
import cursoalgaworks.padroescomportamentais.observer.financeiro.src.main.java.com.algaworks.repository.Lancamentos;

public class LancamentosVencidosJob implements Job {

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        JobDataMap jobDataMap = context.getJobDetail().getJobDataMap();

        Lancamentos lancamentos = (Lancamentos) jobDataMap.get("lancamentos");
        Notificador notificador = (Notificador) jobDataMap.get("notificador");

        List<Lancamento> lancamentosVencidos = lancamentos.todosVencidos();
        notificador.novosLancamentosVencidos(lancamentosVencidos);
    }

}

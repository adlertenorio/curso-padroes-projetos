package cursoalgaworks.padroescomportamentais.observer.financeiro.src.main.java.com.algaworks.listeners;

public interface Listener {

    public void atualizar();

}

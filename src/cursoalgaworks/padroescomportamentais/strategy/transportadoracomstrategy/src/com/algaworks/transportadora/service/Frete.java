package cursoalgaworks.padroescomportamentais.strategy.transportadoracomstrategy.src.com.algaworks.transportadora.service;

public interface Frete {

    public double calcularPreco(int distancia);

}

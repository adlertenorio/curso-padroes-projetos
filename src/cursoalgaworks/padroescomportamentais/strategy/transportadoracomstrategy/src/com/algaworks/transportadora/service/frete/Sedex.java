package cursoalgaworks.padroescomportamentais.strategy.transportadoracomstrategy.src.com.algaworks.transportadora.service.frete;

import cursoalgaworks.padroescomportamentais.strategy.transportadoracomstrategy.src.com.algaworks.transportadora.service.Frete;

public class Sedex implements Frete {

    @Override
    public double calcularPreco(int distancia) {
        return distancia * 1.45 + 12;
    }

}

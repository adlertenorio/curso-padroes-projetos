package cursoalgaworks.padroescomportamentais.strategy.transportadoracomstrategy.src.com.algaworks.transportadora.service;

import cursoalgaworks.padroescomportamentais.strategy.transportadoracomstrategy.src.com.algaworks.transportadora.service.frete.Normal;
import cursoalgaworks.padroescomportamentais.strategy.transportadoracomstrategy.src.com.algaworks.transportadora.service.frete.Sedex;

public enum TipoFrete {

    NORMAL {
        @Override
        public Frete obterFrete() {
            return new Normal();
        }
    },
    SEDEX {
        @Override
        public Frete obterFrete() {
            return new Sedex();
        }
    };

    public abstract Frete obterFrete();

}

package cursoalgaworks.padroescomportamentais.strategy.transportadorasemstrategy.src.com.algaworks.transportadora.service;

public enum TipoFrete {

    NORMAL, SEDEX

}

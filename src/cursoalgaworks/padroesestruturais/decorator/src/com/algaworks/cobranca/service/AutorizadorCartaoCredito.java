package cursoalgaworks.padroesestruturais.decorator.src.com.algaworks.cobranca.service;

import cursoalgaworks.padroesestruturais.decorator.src.com.algaworks.cobranca.model.CartaoCredito;
import cursoalgaworks.padroesestruturais.decorator.src.com.algaworks.cobranca.model.Cliente;

public interface AutorizadorCartaoCredito {

    public void autorizar(Cliente cliente, CartaoCredito cartaoCredito, double valor);

}
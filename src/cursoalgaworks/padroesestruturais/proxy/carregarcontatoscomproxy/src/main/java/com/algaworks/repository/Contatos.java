package cursoalgaworks.padroesestruturais.proxy.carregarcontatoscomproxy.src.main.java.com.algaworks.repository;

public interface Contatos {

    public String buscarPor(String email);

}

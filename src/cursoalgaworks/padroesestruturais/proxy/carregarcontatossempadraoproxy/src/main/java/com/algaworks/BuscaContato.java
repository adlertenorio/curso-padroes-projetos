package cursoalgaworks.padroesestruturais.proxy.carregarcontatossempadraoproxy.src.main.java.com.algaworks;

import cursoalgaworks.padroesestruturais.proxy.carregarcontatossempadraoproxy.src.main.java.com.algaworks.contatos.ContatosXML;
import cursoalgaworks.padroesestruturais.proxy.carregarcontatossempadraoproxy.src.main.java.com.algaworks.repository.Contatos;


public class BuscaContato {

	public static void main(String[] args) {
	    Contatos contatos = new ContatosXML("contatos1.xml", "contatos2.xml");
	    String nome = contatos.buscarPor("jose@email.com");
	    
	    System.out.println(nome);
    }
	
}

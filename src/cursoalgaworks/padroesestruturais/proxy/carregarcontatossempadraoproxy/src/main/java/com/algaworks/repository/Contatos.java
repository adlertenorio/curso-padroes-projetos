package cursoalgaworks.padroesestruturais.proxy.carregarcontatossempadraoproxy.src.main.java.com.algaworks.repository;

public interface Contatos {

	public String buscarPor(String email);
	
}

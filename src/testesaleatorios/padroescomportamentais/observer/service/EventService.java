package testesaleatorios.padroescomportamentais.observer.service;

import testesaleatorios.padroescomportamentais.observer.negocio.Trade;

/**
 * @author Adler Ten�rio
 */
public class EventService {

	public Trade search() {
		return new Trade();
	}
}

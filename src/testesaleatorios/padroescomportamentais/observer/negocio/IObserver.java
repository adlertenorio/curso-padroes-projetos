package testesaleatorios.padroescomportamentais.observer.negocio;

/**
 * @author Adler Ten�rio
 */
public interface IObserver {

	String notificar(Trade trade);
	
	String pageError();
}

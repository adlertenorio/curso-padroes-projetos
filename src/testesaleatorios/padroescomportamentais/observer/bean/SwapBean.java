package testesaleatorios.padroescomportamentais.observer.bean;

import testesaleatorios.padroescomportamentais.observer.negocio.Trade;

/**
 * @author Adler Ten�rio
 */
public class SwapBean {
	
	private Trade trade;

	public Trade getTrade() {
		return trade;
	}

	public void setTrade(Trade trade) {
		this.trade = trade;
		System.out.println("SwapBean notificado !");
	}
}

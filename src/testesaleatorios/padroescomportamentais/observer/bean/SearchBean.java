package testesaleatorios.padroescomportamentais.observer.bean;

import testesaleatorios.padroescomportamentais.observer.negocio.IObserver;
import testesaleatorios.padroescomportamentais.observer.negocio.Trade;
import testesaleatorios.padroescomportamentais.observer.service.EventService;

/**
 * @author Adler Ten�rio
 */
public class SearchBean {

	private EventService eventService;
	private IObserver observer;
	
	public SearchBean(IObserver observer) {
		this.observer = observer;
		this.eventService = new EventService();
	}
	
	public String search() {
		String paginaRetorno = null;
				
		Trade trade = eventService.search();
		paginaRetorno = observer.notificar(trade);
		
		return paginaRetorno;
	}
}

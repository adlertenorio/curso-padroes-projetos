package testesaleatorios.padroescomportamentais.observer.bean;

import testesaleatorios.padroescomportamentais.observer.negocio.IObserver;
import testesaleatorios.padroescomportamentais.observer.negocio.Trade;

/**
 * @author Adler Ten�rio
 */
public class CancellationBean extends SwapBean implements IObserver {

	private SearchBean searchBean;
	
	public CancellationBean() {
		this.searchBean = new SearchBean(this);
	}
	@Override
	public String pageError() {
		return "cancelamentoSearchPage.xhtml";
	}

	@Override
	public String notificar(Trade trade) {
		// Notifica a classe pai o objeto consultado
		super.setTrade(trade);
		return "paginaDetalhe.xhtml";
	}
}

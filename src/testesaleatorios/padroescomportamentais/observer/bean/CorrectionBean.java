package testesaleatorios.padroescomportamentais.observer.bean;

import testesaleatorios.padroescomportamentais.observer.negocio.IObserver;
import testesaleatorios.padroescomportamentais.observer.negocio.Trade;

/**
 * @author Adler Ten�rio
 */
public class CorrectionBean extends SwapBean implements IObserver {

	private SearchBean searchBean;

	public CorrectionBean() {
		this.searchBean = new SearchBean(this);
	}

	@Override
	public String pageError() {
		return "correctionSearchPage.xhtml";
	}

	@Override
	public String notificar(Trade trade) {
		// Notifica a classe pai o objeto consultado
		super.setTrade(trade);
		return "paginaDetalhe.xhtml";
	}
}

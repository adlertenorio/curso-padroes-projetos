package testesaleatorios.padroescomportamentais.observer;

import testesaleatorios.padroescomportamentais.observer.bean.CancellationBean;
import testesaleatorios.padroescomportamentais.observer.bean.CorrectionBean;
import testesaleatorios.padroescomportamentais.observer.negocio.IObserver;
import testesaleatorios.padroescomportamentais.observer.negocio.Trade;

/**
 * @author Adler Ten�rio
 */
public class Tester {
	
	
	public static void main(String[] args) {
		
		Trade trade = new Trade();
		
		IObserver correctionBean = new CorrectionBean();
		System.out.println(correctionBean.notificar(trade));
		System.out.println(correctionBean.pageError());
		
		System.out.println("######################");
		
		
		IObserver cancelationnBean = new CancellationBean();
		System.out.println(cancelationnBean.notificar(trade));
		System.out.println(cancelationnBean.pageError());
	}
}

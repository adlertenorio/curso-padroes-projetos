package cursok19.padroescomportamentais.observer;

/**
 * @author Adler Ten�rio
 */
public class Notificadora implements AcaoObserver {

	private String nome;

	public Notificadora(String nome) {
		this.nome = nome;
	}

	@Override
	public void notificar(Acao acao) {
		System.out.println("Notificadora " + this.nome
				+ " est� sendo notificada pela a��o cujo c�digo � "
				+ acao.getCodigo() + " e o valor est� sendo alterado para "
				+ acao.getValor());
	}
}

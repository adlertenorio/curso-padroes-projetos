package cursok19.padroescomportamentais.observer;

/**
 * @author Adler Ten�rio
 */
public class TesterObserver02 {
	
	public static void main(String[] args) {

		Acao acao = new Acao("12345-x", 10.0);
		
		Notificadora notificadora = new Notificadora("Notificadora 01");
		Notificadora notificadora02 = new Notificadora("Notificadora 02");
		Notificadora notificadora03 = new Notificadora("Notificadora 03");
	
		acao.adicionar(notificadora);
		acao.adicionar(notificadora02);
		acao.adicionar(notificadora03);
		
		acao.setValor(30.0);
		
		///////
	}
}

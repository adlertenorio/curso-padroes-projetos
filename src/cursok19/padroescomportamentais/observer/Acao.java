package cursok19.padroescomportamentais.observer;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Adler Ten�rio
 */
public class Acao {

	private String codigo;
	private double valor;
	private Set<AcaoObserver> observers = new HashSet<AcaoObserver>();

	public Acao(String codigo, double valor) {
		this.codigo = codigo;
		this.valor = valor;
	}

	public void adicionar(AcaoObserver observer) {
		this.observers.add(observer);
	}

	public void remover(AcaoObserver observer) {
		this.observers.remove(observer);
	}

	public String getCodigo() {
		return codigo;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
		notificarTodosObservers();
	}

	private void notificarTodosObservers() {
		for (AcaoObserver observer : observers) {
			observer.notificar(this);
		}
	}
}

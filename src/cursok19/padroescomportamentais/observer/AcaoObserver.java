package cursok19.padroescomportamentais.observer;

/**
 * @author Adler Ten�rio
 */
public interface AcaoObserver {

	public void notificar(Acao acao);
}

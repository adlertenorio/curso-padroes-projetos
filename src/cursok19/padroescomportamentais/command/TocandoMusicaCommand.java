package cursok19.padroescomportamentais.command;

/**
 * @author Adler Ten�rio
 */
public class TocandoMusicaCommand implements ICommand {

	private Player player;
	private String arquivo;

	public TocandoMusicaCommand(Player player, String arquivo) {
		this.player = player;
		this.arquivo = arquivo;
	}

	@Override
	public void executa() {
		this.player.play(arquivo);
	}
}

package cursok19.padroescomportamentais.command;
/**
 * @author Adler Ten�rio
 */
public interface ICommand {

	public void executa();
}

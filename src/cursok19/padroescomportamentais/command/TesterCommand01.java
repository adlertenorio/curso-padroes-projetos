package cursok19.padroescomportamentais.command;

/**
 * @author Adler Ten�rio
 */
public class TesterCommand01 {
	
	public static void main(String[] args) {
		
		Player player = new Player();
		ListaComandos listaComandos = new ListaComandos();
		
		listaComandos.adiciona(new TocandoMusicaCommand(player, "musica01.mp3"));
		listaComandos.adiciona(new TocandoMusicaCommand(player, "musica02.mp3"));
		listaComandos.adiciona(new AumentadoVolumeCommand(player, 2));
		listaComandos.adiciona(new TocandoMusicaCommand(player, "musica03.mp3"));
		listaComandos.adiciona(new DiminuindoVolume(player, 1));
		
		listaComandos.executa();
		
	}
}

package cursok19.padroescomportamentais.command;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Adler Ten�rio
 */
public class ListaComandos {

	List<ICommand> listaComandos = new ArrayList<ICommand>();

	public void adiciona(ICommand comando) {
		this.listaComandos.add(comando);
	}

	public void executa() {
		for (ICommand comando : listaComandos) {
			comando.executa();
		}
	}
}

package cursok19.padroescomportamentais.command;

/**
 * @author Adler Ten�rio
 */
public class Player {
	
	public void play(String arquivo) {
		System.out.println("O " + arquivo + " est� tocando..." );
	}
	
	public void aumentaVolume(int level) {
		System.out.println("Aumentando volume em " + level + " levels");
	}
	
	public void diminuiVolume(int level) {
		System.out.println("Diminuindo volume em " + level + " levels");
	}
}

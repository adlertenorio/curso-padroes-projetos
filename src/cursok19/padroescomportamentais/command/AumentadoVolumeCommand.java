package cursok19.padroescomportamentais.command;
/**
 * @author Adler Ten�rio
 */
public class AumentadoVolumeCommand implements ICommand {
	
	private Player player;
	private int level;

	public AumentadoVolumeCommand(Player player, int level) {
		this.player = player;
		this.level = level;
	}

	@Override
	public void executa() {
		this.player.aumentaVolume(level);
	}
}

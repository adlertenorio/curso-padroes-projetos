package cursok19.padroescomportamentais.iterator;

import java.util.Iterator;

/**
 * @author Adler Ten�rio
 */
public class TesterIterator02 {

	public static void main(String[] args) {

		String[] nomes = new String[4];
		nomes[0] = "Adler Ten�rio";
		nomes[1] = "Rodrigo";
		nomes[2] = "Alex";
		nomes[3] = "Luiz";

		ListaNomes listaNomes = new ListaNomes(nomes);
		Iterator<String> iterator = listaNomes.iterator();

		// Descomentando aqui o primeiro elemento ser� exclu�do
		//		iterator.hasNext();
		//		iterator.remove();

		while (iterator.hasNext()) {
			String nome = iterator.next();
			System.out.println(nome);
		}
		
		System.out.println("------------------------------");
		
		for (String nome : listaNomes) {
			System.out.println(nome);
		}
	}
}

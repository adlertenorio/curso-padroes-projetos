package cursok19.padroescomportamentais.iterator;

import java.util.Iterator;

/**
 * @author Adler Ten�rio
 */
public class ListaNomes implements Iterable<String> {

	private String[] nomes;
	private int lenght;

	public ListaNomes(String[] nomes) {
		this.nomes = nomes;
		this.lenght = this.nomes.length;
	}

	@Override
	public Iterator<String> iterator() {
		return this.new ListaNomesIterator();
	}

	private class ListaNomesIterator implements Iterator<String> {

		private int i = 0;

		public boolean hasNext() {
			return (this.i) < ListaNomes.this.lenght;
		}

		public String next() {
			return ListaNomes.this.nomes[i++];
		}

		public void remove() {
			ListaNomes.this.nomes[i] = null;

			for (int j = i; (j + 1) < ListaNomes.this.lenght; j++) {
				ListaNomes.this.nomes[j] = ListaNomes.this.nomes[j + 1];
			}
			ListaNomes.this.lenght--;
		}
	}
}

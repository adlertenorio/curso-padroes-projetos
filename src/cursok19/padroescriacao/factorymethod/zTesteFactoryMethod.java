package cursok19.padroescriacao.factorymethod;

import cursok19.padroescriacao.factorymethod.fabrica.EmissorMensagemCreator;

/**
 *  @author Adler Ten�rio
 */
public class zTesteFactoryMethod {

	public static void main(String[] args) {

		// FactoryMethod - Objetivo: Encapsular a escolha da classe concreta a
		// ser utilizada na cria��o de objetos de um determinado tipo.

		EmissorMensagemCreator creator = new EmissorMensagemCreator();

		IEmissor emissorSMS = creator.criarEmissor(TipoMensagemEnum.SMS);
		emissorSMS.enviar("K19 Treinamentos");

		IEmissor emissorEmail = creator.criarEmissor(TipoMensagemEnum.EMAIL);
		emissorEmail.enviar("K19 Treinamentos");

		IEmissor emissorJMS = creator.criarEmissor(TipoMensagemEnum.JMS);
		emissorJMS.enviar("K19 Treinamentos");
	}
}

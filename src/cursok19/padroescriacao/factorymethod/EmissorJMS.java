package cursok19.padroescriacao.factorymethod;

/**
 *  @author Adler Ten�rio
 */
public class EmissorJMS implements IEmissor{

	@Override
	public void enviar(String mensagem) {
		System.out.println("Enviando JMS: " + mensagem);
	}
}

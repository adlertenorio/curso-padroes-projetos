package cursok19.padroescriacao.factorymethod;

/**
 *  @author Adler Ten�rio
 */
public interface IEmissor {

	void enviar(String mensagem);
}

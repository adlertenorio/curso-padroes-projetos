package cursok19.padroescriacao.factorymethod;

/**
 *  @author Adler Ten�rio
 */
public class EmissorSMS implements IEmissor{

	@Override
	public void enviar(String mensagem) {
		System.out.println("Enviando SMS: " + mensagem);
	}
}

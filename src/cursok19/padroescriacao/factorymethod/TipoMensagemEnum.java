package cursok19.padroescriacao.factorymethod;

/**
 *  @author Adler Ten�rio
 */
public enum TipoMensagemEnum {

	SMS(0, "SMS"), //
	EMAIL(1, "Email"), //
	JMS(2, "JMS");

	private int codigo;
	private String descricao;

	private TipoMensagemEnum(int codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}

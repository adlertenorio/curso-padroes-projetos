package cursok19.padroescriacao.factorymethod.fabrica;

import cursok19.padroescriacao.factorymethod.EmissorEmail;
import cursok19.padroescriacao.factorymethod.EmissorJMS;
import cursok19.padroescriacao.factorymethod.EmissorSMS;
import cursok19.padroescriacao.factorymethod.IEmissor;
import cursok19.padroescriacao.factorymethod.TipoMensagemEnum;

/**
 *  @author Adler Ten�rio
 */
public class EmissorMensagemCreator {

	public IEmissor criarEmissor(TipoMensagemEnum tipo) {

		IEmissor retorno = null;;
		
		switch (tipo) {

		case SMS:
			retorno = new EmissorSMS();
			break;
		
		case EMAIL:
			retorno = new EmissorEmail();
			break;
			
		case JMS:
			retorno = new EmissorJMS();
			break;
			
		default:
			new IllegalArgumentException("Erro ao criar emissor.");
		}
		
		return retorno;
	}
}

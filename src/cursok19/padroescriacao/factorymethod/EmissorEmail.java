package cursok19.padroescriacao.factorymethod;

/**
 *  @author Adler Ten�rio
 */
public class EmissorEmail implements IEmissor {

	@Override
	public void enviar(String mensagem) {
		System.out.println("Enviando Email: " + mensagem);
	}
}

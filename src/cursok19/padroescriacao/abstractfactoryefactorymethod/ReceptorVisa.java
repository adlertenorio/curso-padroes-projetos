package cursok19.padroescriacao.abstractfactoryefactorymethod;

/**
 *  @author Adler Ten�rio
 */
public class ReceptorVisa implements IReceptor{
	
	@Override
	public String recebe() {
		return "Recebendo Visa";
	}
}

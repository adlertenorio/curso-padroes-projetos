package cursok19.padroescriacao.abstractfactoryefactorymethod;

/**
 *  @author Adler Ten�rio
 */
public interface IReceptor {
	
    public String recebe();

}

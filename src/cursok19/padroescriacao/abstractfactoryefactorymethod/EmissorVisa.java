package cursok19.padroescriacao.abstractfactoryefactorymethod;

/**
 *  @author Adler Ten�rio
 */
public class EmissorVisa implements IEmissor{
	
	@Override
	public void enviar(String mensagem) {
		System.out.println("Enviando Visa: " + mensagem);
	}
}

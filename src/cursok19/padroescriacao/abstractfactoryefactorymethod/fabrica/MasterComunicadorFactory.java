package cursok19.padroescriacao.abstractfactoryefactorymethod.fabrica;

import cursok19.padroescriacao.abstractfactoryefactorymethod.IEmissor;
import cursok19.padroescriacao.abstractfactoryefactorymethod.IReceptor;
import cursok19.padroescriacao.abstractfactoryefactorymethod.TipoCartaoEnum;

/**
 *  @author Adler Ten�rio
 */
public class MasterComunicadorFactory implements IComunicacaoFactory {
	
	EmissorCataoCreator emissor = new EmissorCataoCreator();
	ReceptorCartaoCreator receptor = new ReceptorCartaoCreator();

	@Override
	public IEmissor criarEmissor() {
		return emissor.criarEmissor(TipoCartaoEnum.MASTER);
	}

	@Override
	public IReceptor criarReceptor() {
		return receptor.criarReceptor(TipoCartaoEnum.MASTER);
	}
}

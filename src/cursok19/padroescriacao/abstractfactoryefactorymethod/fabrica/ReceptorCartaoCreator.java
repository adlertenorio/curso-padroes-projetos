package cursok19.padroescriacao.abstractfactoryefactorymethod.fabrica;

import cursok19.padroescriacao.abstractfactoryefactorymethod.IReceptor;
import cursok19.padroescriacao.abstractfactoryefactorymethod.ReceptorMaster;
import cursok19.padroescriacao.abstractfactoryefactorymethod.ReceptorVisa;
import cursok19.padroescriacao.abstractfactoryefactorymethod.TipoCartaoEnum;

/**
 *  @author Adler Ten�rio
 */
public class ReceptorCartaoCreator  {
	
	public IReceptor criarReceptor(TipoCartaoEnum tipo) {

		IReceptor retorno = null;;
		
		switch (tipo) {

		case MASTER:
			retorno = new ReceptorMaster();
			break;
		
		case VISA:
			retorno = new ReceptorVisa();
			break;
			
		default:
			new IllegalArgumentException("Erro ao criar receptor.");
		}
		
		return retorno;
	}

}

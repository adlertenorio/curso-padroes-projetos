package cursok19.padroescriacao.abstractfactoryefactorymethod.fabrica;

import cursok19.padroescriacao.abstractfactoryefactorymethod.EmissorMaster;
import cursok19.padroescriacao.abstractfactoryefactorymethod.EmissorVisa;
import cursok19.padroescriacao.abstractfactoryefactorymethod.IEmissor;
import cursok19.padroescriacao.abstractfactoryefactorymethod.TipoCartaoEnum;

/**
 *  @author Adler Ten�rio
 */
public class EmissorCataoCreator {

	public IEmissor criarEmissor(TipoCartaoEnum tipo) {

		IEmissor retorno = null;;
		
		switch (tipo) {

		case MASTER:
			retorno = new EmissorMaster();
			break;
		
		case VISA:
			retorno = new EmissorVisa();
			break;
			
		default:
			new IllegalArgumentException("Erro ao criar emissor.");
		}
		
		return retorno;
	}
}

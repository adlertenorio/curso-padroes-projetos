package cursok19.padroescriacao.abstractfactoryefactorymethod.fabrica;

import cursok19.padroescriacao.abstractfactoryefactorymethod.IEmissor;
import cursok19.padroescriacao.abstractfactoryefactorymethod.IReceptor;

/**
 *  @author Adler Ten�rio
 */
public interface IComunicacaoFactory {
	
	IEmissor criarEmissor();
	IReceptor criarReceptor();

}

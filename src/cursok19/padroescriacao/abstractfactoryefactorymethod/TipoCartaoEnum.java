package cursok19.padroescriacao.abstractfactoryefactorymethod;

/**
 *  @author Adler Ten�rio
 */
public enum TipoCartaoEnum {

	MASTER(0, "Master"), //
	VISA(1, "Visa");

	private int codigo;
	private String descricao;

	private TipoCartaoEnum(int codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}

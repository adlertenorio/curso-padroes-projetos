package cursok19.padroescriacao.abstractfactoryefactorymethod;

/**
 *  @author Adler Ten�rio
 */
public class EmissorMaster implements IEmissor{

	@Override
	public void enviar(String mensagem) {
		System.out.println("Enviando Master: " + mensagem);
	}
}

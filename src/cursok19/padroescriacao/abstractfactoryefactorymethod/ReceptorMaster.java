package cursok19.padroescriacao.abstractfactoryefactorymethod;

/**
 *  @author Adler Ten�rio
 */
public class ReceptorMaster implements IReceptor{

	@Override
	public String recebe() {
		return "Recebendo Master";
	}
}

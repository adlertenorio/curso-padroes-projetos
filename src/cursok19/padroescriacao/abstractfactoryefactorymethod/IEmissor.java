package cursok19.padroescriacao.abstractfactoryefactorymethod;

/**
 *  @author Adler Ten�rio
 */
public interface IEmissor {

	void enviar(String mensagem);
}

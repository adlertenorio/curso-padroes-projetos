package cursok19.padroescriacao.abstractfactoryefactorymethod;

import cursok19.padroescriacao.abstractfactoryefactorymethod.fabrica.IComunicacaoFactory;
import cursok19.padroescriacao.abstractfactoryefactorymethod.fabrica.MasterComunicadorFactory;

/**
 *  @author Adler Ten�rio
 */
public class zTesteAbstracFactory {
	
	public static void main(String[] args) {
		
		// Abstract Factory - Objetivo: Encapsular a escolha das classes concretas a serem utilizadas na cria��o dos objetos de
		// diversas fam�lias.
		
		// FactoryMethod - Objetivo: Encapsular a escolha da classe concreta a
		// ser utilizada na cria��o de objetos de um determinado tipo.

		
		IComunicacaoFactory comunicadorMaster = new MasterComunicadorFactory();
		IEmissor emissor =	comunicadorMaster.criarEmissor();
		
		emissor.enviar("Valor: 100 - Senha: 123");
		
		IReceptor receptor = comunicadorMaster.criarReceptor();
		String mensagemRecebida = receptor.recebe();
		
		System.out.println(mensagemRecebida);
		
	}
}

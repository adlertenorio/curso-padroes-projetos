package cursok19.padroescriacao.prototype;

import java.util.HashSet;
import java.util.Set;

import org.joda.time.LocalDate;

/**
 *  @author Adler Ten�rio
 */
public class zTestePrototype {
	
	public static void main(String[] args) {
		
		
		Set<String> palavrasChave = new HashSet<String>();
		palavrasChave.add("Vote");
		palavrasChave.add("Daniel Coelho 15");
		palavrasChave.add("Prefeito Recife");
		
		Campanha campanha = new Campanha("Campanha pol�tica", new LocalDate(), palavrasChave);
		
		System.out.println(campanha);
		System.out.println("---------------------------------------");
		
		Campanha campanhaClone = campanha.obterClone();
		
		System.out.println(campanhaClone);
		
		// Test comit eclipse pitang
		
	}
}

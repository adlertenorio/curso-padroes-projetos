package cursok19.padroescriacao.prototype;

import java.util.HashSet;
import java.util.Set;

import org.joda.time.LocalDate;

/**
 *  @author Adler Ten�rio
 */
public class Campanha implements IPrototype<Campanha>{

	private String nome;
	private LocalDate dataNascimento;
	private Set<String> palavrasChave;

	public Campanha() {
		palavrasChave = new HashSet<String>();
	}

	public Campanha(String nome, LocalDate dataNascimento,
			Set<String> palavrasChave) {
		this();
		
		this.nome = nome;
		this.dataNascimento = dataNascimento;
		this.palavrasChave = palavrasChave;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public LocalDate getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public Set<String> getPalavrasChave() {
		return palavrasChave;
	}

	public void setPalavrasChave(Set<String> palavrasChave) {
		this.palavrasChave = palavrasChave;
	}

	@Override
	public Campanha obterClone() {
		return new Campanha(this.nome, this.dataNascimento, this.palavrasChave);
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		
		sb.append("Nome: ");
		sb.append(this.nome);
		sb.append("\n");
		sb.append("Data vencimento: ");
		sb.append(this.dataNascimento);
		sb.append("\n");
		sb.append("Palavras Chaves: ");
		sb.append(this.palavrasChave);
		
		return sb.toString();
	}

}

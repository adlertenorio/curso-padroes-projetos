package cursok19.padroescriacao.prototype;

/**
 *  @author Adler Ten�rio
 */
public interface IPrototype<T> {

	T obterClone();
}

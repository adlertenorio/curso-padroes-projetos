package cursok19.padroescriacao.multiton;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @autor Adler Ten�rio
 */
public class Tema {

	public static final String SKY = "Sky";
	public static final String FIRE = "Fire";

	private String nome;
	private ColorEnum corFundo;
	private ColorEnum corFonte;

	private static Map<String, Tema> mapaTemas = new LinkedHashMap<String, Tema>();

	// Esse bloco est�tico, ser� executado uma �nica vez pelo ClassLoader que � chamado pela JVM para colocar as classes na mem�ria.
	// Independentemente de quantas vezes essa classe for intanciada, esse m�todo s� ser� chamado uma �nica vez.
	static {
		
		System.out.println("Executou bloco est�tico INICIO");
		
		Tema temaSky = new Tema();
		temaSky.setNome(Tema.SKY);
		temaSky.setCorFonte(ColorEnum.AZUL);
		temaSky.setCorFundo(ColorEnum.BRANCO);

		Tema temaFire = new Tema();
		temaFire.setNome(Tema.FIRE);
		temaFire.setCorFonte(ColorEnum.VERDE);
		temaFire.setCorFundo(ColorEnum.PRETO);

		mapaTemas.put(temaSky.getNome(), temaSky);
		mapaTemas.put(temaFire.getNome(), temaFire);
		
		System.out.println("Executou bloco est�tico FIM");
	}

	private Tema() {
	}

	public static Tema obterTemaPorNome(String nomeTema) {
		return Tema.mapaTemas.get(nomeTema);
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public ColorEnum getCorFundo() {
		return corFundo;
	}

	public void setCorFundo(ColorEnum corFundo) {
		this.corFundo = corFundo;
	}

	public ColorEnum getCorFonte() {
		return corFonte;
	}

	public void setCorFonte(ColorEnum corFonte) {
		this.corFonte = corFonte;
	}
}

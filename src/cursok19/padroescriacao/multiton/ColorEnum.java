package cursok19.padroescriacao.multiton;

/**
 * @autor Adler Ten�rio
 */
public enum ColorEnum {

	AZUL(0,"Azul"),
	VERDE(1, "Verde"),
	PRETO(2,"Preto"),
	BRANCO(3,"Branco");

	private Integer codigo;
	private String descricao;

	private ColorEnum(Integer codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;

	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}

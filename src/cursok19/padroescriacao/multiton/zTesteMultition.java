package cursok19.padroescriacao.multiton;

/**
 * @autor Adler Ten�rio
 */
public class zTesteMultition {

	public static void main(String[] args) {
		
		Tema temaSky = Tema.obterTemaPorNome(Tema.SKY);
		
		System.out.println("Nome: " + temaSky.getNome());
		System.out.println("Cor fonte: " + temaSky.getCorFonte());
		System.out.println("Cor fundo: " + temaSky.getCorFundo());
		
		System.out.println("#####################################");

		Tema temaFire = Tema.obterTemaPorNome(Tema.FIRE);

		System.out.println("Nome: " + temaFire.getNome());
		System.out.println("Cor fonte: " + temaFire.getCorFonte());
		System.out.println("Cor fundo: " + temaFire.getCorFundo());
	}
}

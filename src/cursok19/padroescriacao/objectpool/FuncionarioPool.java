package cursok19.padroescriacao.objectpool;

import java.util.ArrayList;
import java.util.List;

/**
 * @autor Adler Ten�rio
 */
public class FuncionarioPool implements IPool<Funcionario>{
	
	private List<Funcionario> funcionarios;

	public FuncionarioPool() {
		funcionarios = new ArrayList<Funcionario>();
		
		funcionarios.add(new Funcionario("Adler Ten�rio"));
		funcionarios.add(new Funcionario("Jos� da Silva"));
		funcionarios.add(new Funcionario("Luiz Pereira"));
	}
	
	@Override
	public Funcionario adquirirRecurso() {
		
		if (this.funcionarios.size() > 0) {
			return this.funcionarios.remove(0);
		} else {
			return null;
		}
	}

	@Override
	public void liberarRecurso(Funcionario recurso) {
		this.funcionarios.add(recurso);
	}
}

package cursok19.padroescriacao.objectpool;

/**
 * @autor Adler Ten�rio
 */
public interface IPool<T> {

	T adquirirRecurso();

	void liberarRecurso(T recurso);

}

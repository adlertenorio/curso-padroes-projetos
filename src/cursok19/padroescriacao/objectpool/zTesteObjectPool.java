package cursok19.padroescriacao.objectpool;


/**
 * @autor Adler Ten�rio
 */
public class zTesteObjectPool {

	public static void main(String[] args) {
		
		IPool<Funcionario> poolFuncionario = new FuncionarioPool();
		
		Funcionario funcionario = poolFuncionario.adquirirRecurso();
		
		while (funcionario != null) {
			System.out.println(funcionario.getNome()); 
			funcionario = poolFuncionario.adquirirRecurso();
		}
	}
}

package cursok19.padroescriacao.singleton;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Adler Ten�rio
 */
public class ConfiguracaoPropriedade {

	private static ConfiguracaoPropriedade instance;
	private Map<String, String> mapaPropriedades;

	private ConfiguracaoPropriedade() {
		mapaPropriedades = new LinkedHashMap<String, String>();
	}

	public static ConfiguracaoPropriedade obterInstancia() {

		if (ConfiguracaoPropriedade.instance == null) {
			ConfiguracaoPropriedade.instance = new ConfiguracaoPropriedade();
		}

		return ConfiguracaoPropriedade.instance;
	}

	public String getPropriedade(String chave) {
		return this.mapaPropriedades.get(chave);
	}
	
	public void adicionarPropriedade(String chave, String valor) {
		this.mapaPropriedades.put(chave, valor);
	}
}

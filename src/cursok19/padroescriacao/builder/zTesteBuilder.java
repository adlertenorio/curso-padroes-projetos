package cursok19.padroescriacao.builder;


/**
 * Vc fornece a l�gica de constru��o de um objeto, para outro objeto constru�-lo, diferente do factory que ele mesmo constroi o objeto.
 * 
 *  @author Adler Ten�rio
 */
public class zTesteBuilder {
	
	public static void main(String[] args) {
		
		BoletoBuilder builder = new BoletoBuilder();
		GeradorBoleto geradorBoleto = new GeradorBoleto(builder);
		
		IBoleto boleto = geradorBoleto.gerarBoleto();
		
		System.out.println(boleto);
	}
}

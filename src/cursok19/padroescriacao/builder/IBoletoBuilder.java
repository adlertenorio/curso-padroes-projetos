package cursok19.padroescriacao.builder;

import java.util.Calendar;

/**
 *  @author Adler Ten�rio
 */
public interface IBoletoBuilder {
	
	/**
	 * Pessoa ou empresa respons�vel pelo pagamento do boleto.
	 */
	void buildSacado(String sacado); 
	
	/**
	 * Pessoa ou empresa que receber� o pagamento do boleto.
	 */
	void buildCedente(String cedente);

	void buildValor(double valor);
	
	void buildVencimento(Calendar dataVencimento);
	
	void buildNumero(int numero);
	
	IBoleto getBoleto();
	
}

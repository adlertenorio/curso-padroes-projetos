package cursok19.padroescriacao.builder;

import java.util.Calendar;

/**
 * Interface criada pois todo os boletos tem que ter esses dados.
 * 
 * @author Adler Ten�rio
 */
public interface IBoleto {

	String getSacado();

	String getCedente();

	double getValor();

	Calendar getDataVencimento();

	int getNumero();

	String toString();

}

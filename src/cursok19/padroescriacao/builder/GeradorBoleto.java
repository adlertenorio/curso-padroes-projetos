package cursok19.padroescriacao.builder;

import java.util.Calendar;

/**
 *  @author Adler Ten�rio
 */
public class GeradorBoleto {
	
	private BoletoBuilder boletoBuilder;
	
	public GeradorBoleto(BoletoBuilder boletoBuilder) {
		this.boletoBuilder = boletoBuilder;
	}
	
	public IBoleto gerarBoleto() {
		
		this.boletoBuilder.buildSacado("Adler Ten�rio");
		this.boletoBuilder.buildCedente("Pitang Agile IT");
		this.boletoBuilder.buildValor(100.0);
		
		Calendar dataVencimento = Calendar.getInstance();
		dataVencimento.add(Calendar.DATE, 30);
		this.boletoBuilder.buildVencimento(dataVencimento);
		
		this.boletoBuilder.buildNumero(123);
				
		return this.boletoBuilder.getBoleto();
	}

}

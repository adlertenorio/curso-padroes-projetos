package cursok19.padroescriacao.builder;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * @author Adler Ten�rio teste commit
 */
public class BoletoBancoBrasil implements IBoleto {

    private final String sacado;
    private final String cedente;
    private final double valor;
    private final Calendar dataVencimento;
    private final int numero;

    public BoletoBancoBrasil(String sacado, String cedente, double valor, Calendar dataVencimento,
            int numero) {
        this.sacado = sacado;
        this.cedente = cedente;
        this.valor = valor;
        this.dataVencimento = dataVencimento;
        this.numero = numero;
    }

    @Override
    public String getSacado() {
        return this.sacado;
    }

    @Override
    public String getCedente() {
        return this.cedente;
    }

    @Override
    public double getValor() {
        return this.valor;
    }

    @Override
    public Calendar getDataVencimento() {
        return this.dataVencimento;
    }

    @Override
    public int getNumero() {
        return this.numero;
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();
        sb.append("Boleto BB ");
        sb.append("Sacado: " + this.sacado);
        sb.append("Cedente: " + this.cedente);
        sb.append("Valor: " + this.valor);
        sb.append("Vencimento: " + this.sacado);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd / MM / yyyy ");

        String format = simpleDateFormat.format(this.dataVencimento.getTime());
        sb.append("Vencimento : " + format);
        System.out.println();
        sb.append("Nosso N�mero : " + this.numero);

        return sb.toString();
    }
}

package cursok19.padroescriacao.builder;

import java.util.Calendar;

/**
 *  @author Adler Ten�rio
 */
public class BoletoBuilder implements IBoletoBuilder {
	
	private String sacado;
	private String cedente;
	private double valor;
	private Calendar dataVencimento;
	private int numero;
	

	@Override
	public void buildSacado(String sacado) {
		this.sacado = sacado;
	}

	@Override
	public void buildCedente(String cedente) {
		this.cedente = cedente;
	}

	@Override
	public void buildValor(double valor) {
		this.valor = valor;
	}

	@Override
	public void buildVencimento(Calendar dataVencimento) {
		this.dataVencimento = dataVencimento;
	}

	@Override
	public void buildNumero(int numero) {
		this.numero = numero;
	}

	@Override
	public IBoleto getBoleto() {
		return new BoletoBancoBrasil(sacado, cedente, valor, dataVencimento, numero);
	}
}

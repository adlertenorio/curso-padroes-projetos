package cursok19.padroesestruturais.decorator;

/**
 * @autor Adler Ten�rio
 */
public class EmissorComCriptografiaDecorator extends AbstractEmissorDecorator {

	public EmissorComCriptografiaDecorator(IEmissor iEmissor) {
		super(iEmissor);
	}

	@Override
	public void enviarMensagem(String mensagem) {
		System.out.println("Enviando mensagem criptrografada...");
		super.getiEmissor().enviarMensagem(criptografarMensagem(mensagem));
	}

	private String criptografarMensagem(String mensagem) {
		return new StringBuffer(mensagem).reverse().toString();
	}
}

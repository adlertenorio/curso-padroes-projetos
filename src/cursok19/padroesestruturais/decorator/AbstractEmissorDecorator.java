package cursok19.padroesestruturais.decorator;

/**
 * @autor Adler Ten�rio
 */
public abstract class AbstractEmissorDecorator implements IEmissor {

	private IEmissor iEmissor;

	public AbstractEmissorDecorator(IEmissor iEmissor) {
		this.iEmissor = iEmissor;
	}
	
	public abstract void enviarMensagem(String mensagem);
	
	public IEmissor getiEmissor() {
		return iEmissor;
	}
}

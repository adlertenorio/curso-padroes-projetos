package cursok19.padroesestruturais.decorator;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;

/**
 * @autor Adler Ten�rio
 */
public class EmissorComCompressaoDecorator extends AbstractEmissorDecorator {

	public EmissorComCompressaoDecorator(IEmissor iEmissor) {
		super(iEmissor);
	}

	@Override
	public void enviarMensagem(String mensagem) {
		System.out.println("Enviando mensagem comprimida...");
		
		String mensagemComprimida;
		
		try {
			mensagemComprimida = comprimirMensagem(mensagem);
		} catch (IOException e) {
			mensagemComprimida = mensagem;
		}
		super.getiEmissor().enviarMensagem(mensagemComprimida);
	}
	
	private String comprimirMensagem(String mensagem) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		DeflaterOutputStream dOut = new DeflaterOutputStream(out, new Deflater());
		
		dOut.write(mensagem.getBytes());
		dOut.close();
		
		return new String(out.toByteArray());
	}
}

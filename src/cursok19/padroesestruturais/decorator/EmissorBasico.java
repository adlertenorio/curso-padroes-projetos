package cursok19.padroesestruturais.decorator;

/**
 * @autor Adler Ten�rio
 */
public class EmissorBasico implements IEmissor {

	@Override
	public void enviarMensagem(String mensagem) {
		System.out.println(mensagem);
	}
}

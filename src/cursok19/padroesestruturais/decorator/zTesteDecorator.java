package cursok19.padroesestruturais.decorator;

/**
 * @autor Adler Ten�rio
 * 
 *        Decorator tem como principal
 *        objetivo a decora��o de classes em tempo de execu��o, isto �,
 *        adicionar novos produtos e/ou novas responsabilidades � objetos
 *        dinamicamente sem alterar o c�digo das classes existentes. Em algumas
 *        situa��es precisamos adicionar responsabilidades � objetos
 *        individuais, e n�o a toda a classe. O padr�o de projeto Decorator
 *        resolve o problema, permitindo que tais responsabilidades sejam
 *        adicionadas individualmente.
 * 
 *        Al�m da caracter�stica principal do padr�o de projeto Decorator que �
 *        adicionar novas responsabilidades dinamicamente, h� tamb�m outras
 *        caracter�sticas importantes que devem ser levadas em considera��o. A
 *        utiliza��o do padr�o permite uma maior flexibilidade e facilidade de
 *        manuten��o de c�digo. Novos produtos/decoradores podem ser facilmente
 *        adicionados na aplica��o, sem a necessidade de alterar o c�digo j�
 *        existente. Tamb�m evita-se classes sobrecarregadas e caracter�sticas
 *        desnecess�rias nas superclasses. Um Decorator oferece a op��o das
 *        responsabilidades e/ou caracter�sticas serem adicionadas
 *        incrementalmente e s� quando necess�rio. Evita-se assim que a
 *        superclasse carregue caracter�sticas e recursos que porventura n�o
 *        ser�o utilizados. Novos decoradores podem ser criados de forma
 *        simples, sem a necessidade de altera��o na superclasse.
 *        Caracter�sticas negativas tamb�m existem neste padr�o de projeto. Em
 *        algumas aplica��es a grande quantidade de pequenos objetos/classes
 *        podem tornar o projeto complexo e de dif�cil compreens�o.
 */
public class zTesteDecorator {

	public static void main(String[] args) {

		IEmissor emissorCriptografia = new EmissorComCriptografiaDecorator(
				new EmissorBasico());
		IEmissor emissorCompressao = new EmissorComCompressaoDecorator(
				new EmissorBasico());

		emissorCriptografia.enviarMensagem("Mensagem");
		emissorCompressao.enviarMensagem("Mensagem");

		IEmissor emissorCriptografiaCompressao = new EmissorComCriptografiaDecorator(
				new EmissorComCompressaoDecorator(new EmissorBasico()));

		emissorCriptografiaCompressao.enviarMensagem("Mensagem");
	}
}

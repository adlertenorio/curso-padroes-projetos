package cursok19.padroesestruturais.decorator;

/**
 * @autor Adler Ten�rio
 */
public interface IEmissor {
	
	public void enviarMensagem(String mensagem);

}

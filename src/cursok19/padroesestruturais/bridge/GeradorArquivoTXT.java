package cursok19.padroesestruturais.bridge;


/**
 * @author Adler Ten�rio
 */
public class GeradorArquivoTXT implements IGeradorArquivo {

	@Override
	public void gerar(String conteudo) {
		System.out.println(conteudo);
	}
}

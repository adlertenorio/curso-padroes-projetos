package cursok19.padroesestruturais.bridge;

/**
 * @author Adler Ten�rio
 * 
 *         Bridge: desacoplar uma abstra��o de sua implementa��o para que ambos
 *         possam variar independentemente
 * 
 *         Implementar um design que permita total desacoplamento entre
 *         interface e implementa��o
 * 
 *         Solu��o: encapsular os detalhes de implementa��o em um objeto que �
 *         um componente da abstra��o
 */
public class zTesteBridge {

	public static void main(String[] args) {

		IDocumento recibo = new Recibo("k19 Treinamentos", "Adler Ten�rio", 100.0,
				new GeradorArquivoTXT());
		recibo.gerarArquivo();

	}
}

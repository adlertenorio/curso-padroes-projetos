package cursok19.padroesestruturais.bridge;

/**
 * @author Adler Ten�rio
 */
public interface IDocumento {
	
	void gerarArquivo();

}

package cursok19.padroesestruturais.bridge;


/**
 * @author Adler Ten�rio
 */
public class Recibo implements IDocumento {

	private String emissor;
	private String favorecido;
	private double valor;
	private IGeradorArquivo iGeradorArquivo;
			
	public Recibo(String emissor, String favorecido, double valor, IGeradorArquivo iGeradorArquivo) {
		this.emissor = emissor;
		this.favorecido = favorecido;
		this.valor = valor;
		this.iGeradorArquivo = iGeradorArquivo;
	}

	@Override
	public void gerarArquivo() {
		
		StringBuilder sb = new StringBuilder();
		sb.append("Recibo ");
		sb.append("\n");
		sb.append("Empresa: ");
		sb.append(this.emissor);
		sb.append("\n");
		sb.append("Cliente: ");
		sb.append(this.favorecido);
		sb.append("\n");
		sb.append("Valor: ");
		sb.append(this.valor);
		
		this.iGeradorArquivo.gerar(sb.toString());
	}
		

	public String getEmissor() {
		return emissor;
	}

	public void setEmissor(String emissor) {
		this.emissor = emissor;
	}

	public String getFavorecido() {
		return favorecido;
	}

	public void setFavorecido(String favorecido) {
		this.favorecido = favorecido;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public IGeradorArquivo getiGeradorArquivo() {
		return iGeradorArquivo;
	}

	public void setiGeradorArquivo(IGeradorArquivo iGeradorArquivo) {
		this.iGeradorArquivo = iGeradorArquivo;
	}

}

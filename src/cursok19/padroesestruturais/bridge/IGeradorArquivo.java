package cursok19.padroesestruturais.bridge;

/**
 * @author Adler Ten�rio
 */
public interface IGeradorArquivo {
	
	void gerar(String conteudo);

}

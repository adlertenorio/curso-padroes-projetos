package cursok19.padroesestruturais.composite;

/**
 * @autor Adler Ten�rio
 * 
 *        :: Composite
 * 
 *        Composite(Objecto Composto) � um padr�o de projeto que permite que um
 *        objeto seja constituido de outros objetos semelhantes a ele formando
 *        uma hierarquia.[1].
 * 
 *        Utilizando o padr�o Composite � simples criar uma hierarquia de
 *        objetos semelhantes. Semelhantes, significa aqui, objetos que
 *        implementam uma interface comum. O padr�o simplifica o tratamentos de
 *        cada elemento da hierarquia ao tratar a todos como implementa��es da
 *        mesma interface.
 * 
 *        :: O problema
 * 
 *        Voc� quer construir um objeto que seja construido de outros objetos
 *        tal que, um ou mais objetos desses podem ser do mesmo tipo do objeto
 *        construido. O objeto � construido por objetos que cont�m uma cole��o
 *        de outros objetos. Os quais cont�m uma cole��o de outros objetos, e
 *        assim sucessivamente. Contudo esses objetos n�o s�o quaisquer, eles
 *        compatilham uma interface comum. Por exemplo, um objeto do tipo Forma.
 *        Existem v�rias formas, mas todas elas podem ser construidas pela
 *        composi��o de outras formas. Um ou mais formas s�o primitivas, ou
 *        seja, n�o s�o construidas a partir de nenhuma forma, outras ser�o. O
 *        exemplo mais simples � a forma Linha que representa uma semi-reta. Um
 *        Triangulo � composto por tr�s Linha. Um quadrado por quatro ,e etc� Um
 *        circulo n�o � represent�vel por um conjunto finito de linhas, logo
 *        precisamos de uma forma primitiva Circulo. Como construir os objetos
 *        do tipo Forma para que eles obde�am a esta hirarquia � o que o padr�o
 *        Composite resolve.
 * 
 *        :: A solu��o
 * 
 *        O padr�o prop�e que se construa uma interface ou classe abstrata que
 *        representa o tipo de objeto na hierarquia. Para que o padr�o possa ser
 *        aplicado tem que existir esta interface ou classe (vou apenas usar o
 *        termo interface para simplificar).
 * 
 *        Objetos que implementam esta interface podem ser de dois tipos:
 *        primitivos e compostos. Os objetos primitivos s�o aqueles que n�o se
 *        podem construir com base em outros objetos do mesmo tipo. Os
 *        compostos, s�o, claro, aqueles que se constroem com base num conjunto
 *        de outros objetos do mesmo tipo.
 * 
 */
public class zTesteComposite {

	public static void main(String[] args) {

		ITrecho iTrecho = new TrechoAndando("Av. Agamenon Magalh�es", 100.0);
		ITrecho iTrecho02 = new TrechoCarro("Av. Rosa e Silva", 200.0);
		ITrecho iTrecho03 = new TrechoAndando("Av. Abdias de Carvalho", 300.0);

		CaminhoPercorrido caminho = new CaminhoPercorrido();
		caminho.adicionar(iTrecho);
		caminho.adicionar(iTrecho02);

		System.out.println("##### Caminho 01 #####");

		caminho.imprime();

		System.out.println("##### Caminho 02 #####");

		CaminhoPercorrido caminho02 = new CaminhoPercorrido();
		caminho02.adicionar(caminho); // Aqui est� o grande ponto positivo desse
										// padr�o.
		caminho02.adicionar(iTrecho03);

		caminho02.imprime();
	}
}

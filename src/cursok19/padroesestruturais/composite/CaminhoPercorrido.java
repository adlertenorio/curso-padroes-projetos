package cursok19.padroesestruturais.composite;

import java.util.ArrayList;
import java.util.List;

/**
 * @autor Adler Ten�rio
 */
public class CaminhoPercorrido implements ITrecho {

	private List<ITrecho> listaTrechos;

	public CaminhoPercorrido() {
		listaTrechos = new ArrayList<ITrecho>();
	}

	@Override
	public void imprime() {
		for (ITrecho iTrecho : this.listaTrechos) {
			iTrecho.imprime();
		}
	}

	public void adicionar(ITrecho iTrecho) {

		if (iTrecho != null) {
			listaTrechos.add(iTrecho);
		}
	}

	public void remover(ITrecho iTrecho) {

		if (iTrecho != null) {
			listaTrechos.remove(iTrecho);
		}
	}
}

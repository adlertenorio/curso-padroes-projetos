package cursok19.padroesestruturais.composite;

/**
 * @autor Adler Ten�rio
 */
public class TrechoCarro implements ITrecho {
	
	private String direcao;
	private double distancia;

	public TrechoCarro(String direcao, double distancia) {
		super();
		this.direcao = direcao;
		this.distancia = distancia;
	}

	@Override
	public void imprime() {

		System.out.println("Dirija at� a dire��o:");
		System.out.println(this.direcao);
		System.out.println("A dist�ncia percorrida ser� de " + this.distancia
				+ " metros.");
	}

	public String getDirecao() {
		return direcao;
	}

	public void setDirecao(String direcao) {
		this.direcao = direcao;
	}

	public double getDistancia() {
		return distancia;
	}

	public void setDistancia(double distancia) {
		this.distancia = distancia;
	}
}

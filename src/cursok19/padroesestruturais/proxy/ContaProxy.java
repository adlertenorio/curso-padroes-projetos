package cursok19.padroesestruturais.proxy;

/**
 * @author Adler Ten�rio
 */
public class ContaProxy implements IConta {
	
	private IConta iConta;
	
	public ContaProxy(IConta iConta) {
		this.iConta = iConta;
	}

	@Override
	public void depositar(double valor) {
		System.out.println("Efetuando deposito no valor de " + valor);
		this.iConta.depositar(valor);
		System.out.println("Deposito no valor de " + valor + " efetuado com sucesso.");

	}

	@Override
	public void sacar(double valor) {
		System.out.println("Efetuando saque no valor de " + valor);
		this.iConta.sacar(valor);
		System.out.println("Saque no valor de " + valor + " efetuado com sucesso.");
	}

	@Override
	public double obterSaldo() {
		return this.iConta.obterSaldo();
	}

}

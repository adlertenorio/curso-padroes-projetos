package cursok19.padroesestruturais.proxy;

/**
 * @author Adler Ten�rio
 */
public interface IConta {
	
	void depositar(double valor);
	void sacar(double valor);
	double obterSaldo();

}

package cursok19.padroesestruturais.proxy;

/**
 * @author Adler Ten�rio
 */
public class ContaPadrao implements IConta {

	private double saldo;
	
	@Override
	public void depositar(double valor) {
		this.saldo += valor;
	}

	@Override
	public void sacar(double valor) {
		this.saldo -= valor;
	}

	@Override
	public double obterSaldo() {
		return this.getSaldo();
	}

	public double getSaldo() {
		return this.saldo;
	}
}

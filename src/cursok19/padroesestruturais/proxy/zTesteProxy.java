package cursok19.padroesestruturais.proxy;

/**
 * @author Adler Ten�rio
 * 
 *         O proxy, � uma entidade que decis�o, ou seja, decide alguma coisa,
 *         seja ela uma requisi��o ou algo do tipo.
 */
public class zTesteProxy {

	public static void main(String[] args) {
		
		IConta contaProxy = new ContaProxy(new ContaPadrao());

		contaProxy.depositar(100.00);
		contaProxy.sacar(11);

		System.out.println(contaProxy.obterSaldo());
	}
}

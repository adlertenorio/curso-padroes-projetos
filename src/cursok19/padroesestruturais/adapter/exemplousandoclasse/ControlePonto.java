package cursok19.padroesestruturais.adapter.exemplousandoclasse;

import org.joda.time.LocalDate;

/**
 * @author Adler Ten�rio
 */
public class ControlePonto {

	public void registraEntrada(Funcionario funcionario) {
		System.out.println("Entrada: " + funcionario.getNome() + " �s "
				+ new LocalDate().toDateTimeAtCurrentTime().toLocalTime());
	}
	
	public void registraSaida(Funcionario funcionario) {
		System.out.println("Sa�da: " + funcionario.getNome() + " �s "
				+ new LocalDate().toDateTimeAtCurrentTime().toLocalTime());
	}

}

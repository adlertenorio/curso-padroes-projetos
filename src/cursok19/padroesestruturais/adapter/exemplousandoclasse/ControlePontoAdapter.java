package cursok19.padroesestruturais.adapter.exemplousandoclasse;

/**
 * @author Adler Ten�rio
 */
public class ControlePontoAdapter extends ControlePonto {
	
	private ControlePontoNovo controlePontoNovo;
	
	public ControlePontoAdapter() {
		this.controlePontoNovo = new ControlePontoNovo();
	}
	
	@Override
	public void registraEntrada(Funcionario funcionario) {
		System.out.println("M�todo ENTRADA adapter");
		controlePontoNovo.registra(funcionario, true);
	}
	
	@Override
	public void registraSaida(Funcionario funcionario) {
		System.out.println("M�todo SA�DA adapter");
		controlePontoNovo.registra(funcionario, false);
	}

}

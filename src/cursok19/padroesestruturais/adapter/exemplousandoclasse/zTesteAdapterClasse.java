package cursok19.padroesestruturais.adapter.exemplousandoclasse;

/**
 * @author Adler Ten�rio
 * 
 *         O Padr�o de Projeto Adapter, tamb�m conhecido como Wrapper � um
 *         padr�o de estrutura. Esse padr�o de projeto tem como finalidade
 *         �adaptar� a interface de uma classe, permitindo que classes com
 *         interfaces incompat�veis possam interagir. O Adapter permite que um
 *         objeto cliente utilize servi�os de outros objetos com interfaces
 *         diferentes por meio de uma interface �nica.
 * 
 */
public class zTesteAdapterClasse {

	public static void main(String[] args) throws InterruptedException {

		// ControlePonto controle = new ControlePonto(); Antes do adapter
		ControlePonto controle = new ControlePontoAdapter();
		Funcionario funcionario = new Funcionario("Adler Ten�rio");

		controle.registraEntrada(funcionario);
		Thread.sleep(3000);
		controle.registraSaida(funcionario);

	}
}

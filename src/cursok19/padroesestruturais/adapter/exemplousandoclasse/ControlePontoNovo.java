package cursok19.padroesestruturais.adapter.exemplousandoclasse;

import org.joda.time.LocalDate;

/**
 * @author Adler Ten�rio
 */
public class ControlePontoNovo {

	public void registra(Funcionario funcionario, boolean entrada) {

		if (entrada) {
			System.out.println("Entrada: " + funcionario.getNome() + " �s "
					+ new LocalDate().toDateTimeAtCurrentTime().toLocalTime());
		} else {
			System.out.println("Sa�da: " + funcionario.getNome() + " �s "
					+ new LocalDate().toDateTimeAtCurrentTime().toLocalTime());
		}
	}
}

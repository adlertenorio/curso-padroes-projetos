package cursok19.padroesestruturais.adapter.exemplousandointerface;

/**
 * 
 * @author Adler Ten�rio
 */
public class Metraladora implements IArma {

	public void mirar() {
		System.out.println("Mirando Metraladora ");
	}

	public void atirar() {
		System.out.println("Atirando Metraladora");
	}

	public void carregar() {
		System.out.println("Carregando Metraladora");
	}
}

package cursok19.padroesestruturais.adapter.exemplousandointerface;

/**
 * @author Adler Ten�rio
 */
public class BazucaAdapter extends Bazuca implements IArma{

	@Override
	public void mirar() {
		super.mirarAltoAlcance();
	}

	@Override
	public void atirar() {
		super.atirarMissil();
		
	}

	@Override
	public void carregar() {
		super.carregarMissil();
	}
}

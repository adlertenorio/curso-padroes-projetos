package cursok19.padroesestruturais.adapter.exemplousandointerface;

/**
 * @author Adler Ten�rio
 * 
 *         O Padr�o de Projeto Adapter, tamb�m conhecido como Wrapper � um
 *         padr�o de estrutura. Esse padr�o de projeto tem como finalidade
 *         �adaptar� a interface de uma classe, permitindo que classes com
 *         interfaces incompat�veis possam interagir. O Adapter permite que um
 *         objeto cliente utilize servi�os de outros objetos com interfaces
 *         diferentes por meio de uma interface �nica.
 * 
 */
public class zTesteAdapterInterface {
    
	public static void main(String[] args) {
     
    	IArma metraladora = new Metraladora();
        Personagem personagem = new Personagem("Adler Ten�rio", metraladora);

        System.out.println(personagem.getNome() +  "  usando Metraladora");
        personagem.mirar();
        personagem.atirar();
        personagem.carregarArma();

        IArma pistola = new Pistola();
        personagem.setArma(pistola);

        System.out.println("\n" + personagem.getNome() +  " usando Pistola");
        personagem.mirar();
        personagem.atirar();
        personagem.carregarArma();

        IArma rifle = new Rifle();
        personagem.setArma(rifle);

        System.out.println("\n" + personagem.getNome() +  " usando Rifle");
        personagem.mirar();
        personagem.atirar();
        personagem.carregarArma();
        
        IArma bazuca = new BazucaAdapter();
        personagem.setArma(bazuca);
        
        System.out.println("\n" + personagem.getNome() +  " usando Bazuca");
        personagem.mirar();
        personagem.atirar();
        personagem.carregarArma();

    }
}

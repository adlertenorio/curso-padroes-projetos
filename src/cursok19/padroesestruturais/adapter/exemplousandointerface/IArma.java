
package cursok19.padroesestruturais.adapter.exemplousandointerface;

/**
 *
 * @author pagliares
 */
public interface IArma {
  
	public abstract void mirar();
    public abstract void atirar();
    public abstract void carregar();

}

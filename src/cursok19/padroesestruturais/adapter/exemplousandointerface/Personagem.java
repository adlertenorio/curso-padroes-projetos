
package cursok19.padroesestruturais.adapter.exemplousandointerface;

/**
 * 
 * @author Adler Ten�rio
 */
public class Personagem {
	
	private String nome;
	private IArma arma;

	public Personagem(String nome, IArma arma) {
		this.nome = nome;
		this.arma = arma;
	}

	public void mirar() {
		this.arma.mirar();
	}
	
	public void atirar() {
		this.arma.atirar();
	}

	public void carregarArma() {
		this.arma.carregar();
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public IArma getArma() {
		return arma;
	}

	public void setArma(IArma arma) {
		this.arma = arma;
	}

}

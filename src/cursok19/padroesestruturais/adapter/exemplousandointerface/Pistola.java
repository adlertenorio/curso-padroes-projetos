package cursok19.padroesestruturais.adapter.exemplousandointerface;

/**
 * 
 * @author Adler Ten�rio
 */
public class Pistola implements IArma {

	public void mirar() {
		System.out.println("Mirando Pistola ");
	}

	public void atirar() {
		System.out.println("Atirando Pistola");
	}

	public void carregar() {
		System.out.println("Carregando Pistola");
	}
}

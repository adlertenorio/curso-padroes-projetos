package cursok19.padroesestruturais.adapter.exemplousandointerface;

/**
 * @author Adler Ten�rio
 */
public class Rifle implements IArma {

	public void mirar() {
		System.out.println("Mirando Rifle ");
	}

	public void atirar() {
		System.out.println("Atirando Rifle");
	}

    public void carregar() {
    	System.out.println("Carregando Rifle");
    }
}

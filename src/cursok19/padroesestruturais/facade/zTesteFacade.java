package cursok19.padroesestruturais.facade;

/**
 * @author Adler Ten�rio
 *
 * :: Explica��o
 * 
 * Facade/Fa�ade/Fachada - Prov� uma interface unificada para um conjunto de
 * interfaces em um subsitema. Facade ou Fa�ade ou Fachada define uma
 * interface de mais alto n�vel, que torna o subsistema mais f�cil de se
 * usar
 * 
 * 
 * :: Motiva��o
 * 
 * Estruturar um sistema em subsistemas ajuda a reduzir a complexidade Um
 * objetivo de projeto comum � minimizar a comunica��o e as depend�ncias
 * entre subsistemas.
 * 
 * Uma forma de se alcan�ar isto � introduzindo um objeto FACADE, que prov�
 * uma interface �nica e simplificada �s facilidades mais gerais de um
 * subsistema.
 * 
 * 
 * :: Problema
 * 
 * Problemas: Se houver manuten��es nas classes do sub-sistema, os clientes
 * ser�o afetados. Acoplamento Forte, Dificuldade de manuten��o e extens�o
 * 
 * :: Solu��o
 * 
 * Implementar um classe Fa�ade; Fa�ade atua na intermedia��o (orquestra��o,
 * coordena��o) da funcionalidade desejada pelo cliente Classe Fa�ade � uma
 * interface de alto n�vel (provedor de servi�os), ou seja, abstrai o
 * detalhes do sub-sistema para os clientes.
 * 
 * 
 * :: Vantagens
 * 
 * Mudan�as na classes podem afetar fa�ade mas n�o o cliente; Diminui��o do
 * acoplamento do cliente com a classes do subsistema; Cliente usa o servi�o
 * do fa�ade e n�o acopla-se diretamente com a implementa��o do servi�o
 * (classes do sub-sistema)
 * 
 * 
 * :: Consequ�ncias e benef�cios
 * 
 * Separa clientes dos componentes do subsistema, reduzindo o n�mero de
 * objetos que os clientes lidam e tornando o subsistema mais f�cil de se
 * usar; 
 * Promove acoplamento fraco entre o subsistema e seus clientes;
 * Acoplamento fraco permite que se mude os componentes de um subsistema sem
 * afetar os clientes; 
 * Al�m de ajudar a reduzir as depend�ncias N�o impede
 * que aplica��es usem diretamente as classes do subsistema, caso
 * necess�rio.
 * 
 */
public class zTesteFacade {

	

	public static void main(String[] args) {

		PedidoFacade facade = new PedidoFacade(new Estoque(), new Financeiro(),
				new PosVenda());

		facade.registrarPedido(new Pedido("Notebook", "Adler Ten�rio", "Rua A"));
	}

}

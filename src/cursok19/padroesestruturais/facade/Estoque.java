package cursok19.padroesestruturais.facade;

import org.joda.time.LocalDate;

/**
 * @author Adler Ten�rio
 */
public class Estoque {
	
	public void enviarProduto(String produto, String enderecoEntrega) {
		
		StringBuilder sb = new StringBuilder();
		sb.append("Produto ");
		sb.append(produto);
		sb.append(" ser� entregue ");
		sb.append(enderecoEntrega);
		sb.append(" at� as 18hs do dia ");
		sb.append( new LocalDate());
		
		System.out.println(sb.toString());
	}
}

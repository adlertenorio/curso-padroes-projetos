package cursok19.padroesestruturais.facade;

/**
 * @author Adler Ten�rio
 */
public class Pedido {
	
	private String produto;
	private String cliente;
	private String endecoEntrega;

	public Pedido(String produto, String cliente, String endecoEntrega) {
		super();
		this.produto = produto;
		this.cliente = cliente;
		this.endecoEntrega = endecoEntrega;
	}

	public String getProduto() {
		return produto;
	}
	public void setProduto(String produto) {
		this.produto = produto;
	}
	public String getCliente() {
		return cliente;
	}
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}
	public String getEndecoEntrega() {
		return endecoEntrega;
	}
	public void setEndecoEntrega(String endecoEntrega) {
		this.endecoEntrega = endecoEntrega;
	}
}

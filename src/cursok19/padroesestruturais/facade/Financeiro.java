package cursok19.padroesestruturais.facade;

/**
 * @author Adler Ten�rio
 */
public class Financeiro {
	
	public void faturar(String cliente, String produto) {
		
		StringBuilder sb = new StringBuilder();
		sb.append(":::Fatura \n");
		sb.append(" Cliente: ");
		sb.append(cliente);
		sb.append("\n");
		sb.append(" Produto: ");
		sb.append(produto);
		
		System.out.println(sb.toString());
	}
}

package cursok19.padroesestruturais.facade;

import org.joda.time.LocalDate;

/**
 * @author Adler Ten�rio
 */
public class PosVenda {
	
	public void agendarContato(String cliente, String produto) {
	
		StringBuilder sb = new StringBuilder();
		sb.append("Entrar em contato com o cliente ");
		sb.append(cliente);
		sb.append(" sobre o produto ");
		sb.append(produto);
		sb.append(" no dia ");
		sb.append( new LocalDate());
	}
}

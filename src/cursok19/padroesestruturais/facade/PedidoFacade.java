package cursok19.padroesestruturais.facade;

/**
 * @author Adler Ten�rio
 */
public class PedidoFacade {

	private Estoque estoque;
	private Financeiro financeiro;
	private PosVenda posVenda;
	
	public PedidoFacade(Estoque estoque, Financeiro financeiro,
			PosVenda posVenda) {
		super();
		this.estoque = estoque;
		this.financeiro = financeiro;
		this.posVenda = posVenda;
	}
	
	public void registrarPedido(Pedido pedido) {
		String produto = pedido.getProduto();
		String cliente = pedido.getCliente();		
		
		this.estoque.enviarProduto(produto, pedido.getEndecoEntrega());
		this.financeiro.faturar(cliente, produto);
		this.posVenda.agendarContato(cliente, produto);
	}

	public Estoque getEstoque() {
		return estoque;
	}

	public void setEstoque(Estoque estoque) {
		this.estoque = estoque;
	}

	public Financeiro getFinanceiro() {
		return financeiro;
	}

	public void setFinanceiro(Financeiro financeiro) {
		this.financeiro = financeiro;
	}

	public PosVenda getPosVenda() {
		return posVenda;
	}

	public void setPosVenda(PosVenda posVenda) {
		this.posVenda = posVenda;
	}
}

package cursok19.padroesestruturais.flyweight;

import java.util.Arrays;

/**
 * @author Adler Ten�rio
 */
public class TemaHifen implements ITemaFlyweight {

	@Override
	public void imprime(String titulo, String texto) {

		System.out.println("----------" + titulo + "----------");
		System.out.println(texto);
		
		char[] rodape = new char[22 + titulo.length()];
		Arrays.fill(rodape, '-');
		
		System.out.println(rodape);
	}
}

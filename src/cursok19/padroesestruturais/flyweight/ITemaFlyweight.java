package cursok19.padroesestruturais.flyweight;

/**
 * @author Adler Ten�rio
 */
public interface ITemaFlyweight {

	
	void imprime(String titulo, String texto);
}

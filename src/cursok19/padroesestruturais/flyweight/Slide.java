package cursok19.padroesestruturais.flyweight;

/**
 * @author Adler Ten�rio
 */
public class Slide {

	private ITemaFlyweight iTema;
	private String titulo;
	private String texto;
	
	
	public Slide(ITemaFlyweight iTema, String titulo, String texto) {
		super();
		this.iTema = iTema;
		this.titulo = titulo;
		this.texto = texto;
	}
	
	public void imprime() {
		iTema.imprime(titulo, texto);
	}
	
	public ITemaFlyweight getiTema() {
		return iTema;
	}
	public void setiTema(ITemaFlyweight iTema) {
		this.iTema = iTema;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
	
}

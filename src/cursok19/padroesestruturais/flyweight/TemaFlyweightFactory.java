package cursok19.padroesestruturais.flyweight;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Adler Ten�rio
 */
public class TemaFlyweightFactory {

	private static Map<Class<? extends ITemaFlyweight>, ITemaFlyweight> mapaTemas 
									= new HashMap<Class<? extends ITemaFlyweight>, ITemaFlyweight>();

	public static final Class<TemaHifen> TEMA_HIFEN = TemaHifen.class;
	public static final Class<TemaAsterisco> TEMA_ASTERISCO = TemaAsterisco.class;
	public static final Class<TemaK19> TEMA_K19 = TemaK19.class;

	public static ITemaFlyweight obterTema(Class<? extends ITemaFlyweight> clazz) {

		if (!mapaTemas.containsKey(clazz)) {
			try {
				mapaTemas.put(clazz, clazz.newInstance());
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}

		return mapaTemas.get(clazz);
	}
}

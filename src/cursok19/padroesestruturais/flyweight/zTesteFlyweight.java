package cursok19.padroesestruturais.flyweight;

/**
 * @author Adler Ten�rio
 */
public class zTesteFlyweight {

	public static void main(String[] args) {
		ApresentacaoSlides apresentacao = new ApresentacaoSlides();

		apresentacao.adicionaSlide(new Slide(TemaFlyweightFactory
				.obterTema(TemaFlyweightFactory.TEMA_K19),
				"APRESENTACAO SLIDES K19",
				"...."));

		apresentacao.adicionaSlide(new Slide(TemaFlyweightFactory
				.obterTema(TemaFlyweightFactory.TEMA_ASTERISCO),
				"CURSO PADR�ES DE PROJETO",
				"Esse curso visa mostrar o que � um padr�o..."));
		
		apresentacao.adicionaSlide(new Slide(TemaFlyweightFactory
				.obterTema(TemaFlyweightFactory.TEMA_HIFEN),
				"CURSO JSF",
				"Esse curso visa mostrar o que � um JSF..."));
		
		
		apresentacao.imprime();
	}
}

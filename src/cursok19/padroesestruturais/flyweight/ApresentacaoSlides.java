package cursok19.padroesestruturais.flyweight;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Adler Ten�rio
 */
public class ApresentacaoSlides {

	private List<Slide> listaSlides = new ArrayList<Slide>();

	public void adicionaSlide(Slide slide) {

		if (slide != null) {
			listaSlides.add(slide);
		}
	}

	public void imprime() {
		for (Slide slide : listaSlides) {
			slide.imprime();
			System.out.println();
		}
	}
}

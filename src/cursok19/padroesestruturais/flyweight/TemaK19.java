package cursok19.padroesestruturais.flyweight;

import java.util.Arrays;

/**
 * @author Adler Ten�rio
 */
public class TemaK19 implements ITemaFlyweight {

	@Override
	public void imprime(String titulo, String texto) {
		
		
		System.out.println("##########" + titulo + "##########");
		System.out.println(texto);
		
		char [ ] rodapeE = new char [(int) Math.floor((6 + titulo.length())/2.0)];
		char [ ] rodapeD = new char [(int) Math.ceil((6 + titulo.length())/2.0)];
		
		Arrays.fill(rodapeE, '#');
		Arrays.fill(rodapeD, '#');

		System.out.println(new String(rodapeE) + "wwww.k19.com.br" + new String(rodapeD));
		
	}

}

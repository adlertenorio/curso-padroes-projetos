package cursocesar.padroesestruturais.facade;

/**
 * @author Adler Ten�rio
 */
public class TesterExemploFacade {
	
	public static void main(String[] args) {

		Facade.getInstance().playTheBass();
		Facade.getInstance().playTheBass();
		System.out.println(Facade.getInstance().playTheGuitar());
		Facade.getInstance().playTheDrums();
		Facade.getInstance().playTheDrums();
		Facade.getInstance().playTheDrums();
		Facade.getInstance().playTheDrums();
		System.out.println(Facade.getInstance().playTheGuitar());
		Facade.getInstance().playTheBass();
		Facade.getInstance().playTheBass();
		System.out.println(Facade.getInstance().playTheGuitar());
		Facade.getInstance().playTheDrums();
		System.out.println(Facade.getInstance().playTheGuitar());
		System.out.println(Facade.getInstance().playTheGuitar());
		Facade.getInstance().playTheBass();
		Facade.getInstance().playTheDrums();
		Facade.getInstance().playTheBass();
		Facade.getInstance().playTheBass();
		Facade.getInstance().playTheBass();
		System.out.println(Facade.getInstance().playTheGuitar());
		System.out.println(Facade.getInstance().playTheGuitar());
		Facade.getInstance().playTheDrums();
		Facade.getInstance().playTheDrums();
		System.out.println(Facade.getInstance().playTheGuitar());
		
	}
}

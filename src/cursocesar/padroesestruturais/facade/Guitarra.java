package cursocesar.padroesestruturais.facade;

/**
 * @author Adler Ten�rio
 */
public class Guitarra {
	
	public String playTheGuitar() {
		return "Som guitarra";
	}
}

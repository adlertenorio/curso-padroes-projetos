package cursocesar.padroesestruturais.facade;
/**
 * @author Adler Ten�rio
 */
public class Bateria {

	private static Bateria instance;

	private Bateria() {
	}

	public static Bateria getInstance() {
		if (instance == null) {
			instance = new Bateria();
		}

		return instance;
	}

	public void playTheDrums() {
		System.out.println("Som da bateria");
	}
}

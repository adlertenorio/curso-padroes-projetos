package cursocesar.padroesestruturais.facade;
/**
 * @author Adler Ten�rio
 */
public class Facade {

	private static Facade instance;

	private Facade() {}

	public static Facade getInstance() {
		if (instance == null) {
			instance = new Facade();
		}

		return instance;
	}

	public void playTheBass() {
		new Baixo().playTheBass();
	}

	public void playTheDrums() {
		Bateria.getInstance().playTheDrums();
	}

	public String playTheGuitar() {
		return new Guitarra().playTheGuitar();
	}
}

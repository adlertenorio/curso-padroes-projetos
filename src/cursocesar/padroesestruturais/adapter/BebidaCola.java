package cursocesar.padroesestruturais.adapter;

/**
 * @author Adler Ten�rio
 */
public class BebidaCola {
	
	private String nome;

	public BebidaCola(String nome){
		this.nome = nome;
	}
	public void beber() {
		System.out.println("Bebida cola: " + this.nome);
	}
}

package cursocesar.padroesestruturais.adapter;


/**
 * @author Adler Ten�rio
 */
public class BebidaFrutaAdapter implements IBebida {
	
	private BebidaFruta bebidaFruta;

	public BebidaFrutaAdapter(BebidaFruta bebidaFruta){
		this.bebidaFruta = bebidaFruta;
	}
	
	@Override
	public void beber() {
		bebidaFruta.agitar();
		bebidaFruta.bebendo();
	}
}

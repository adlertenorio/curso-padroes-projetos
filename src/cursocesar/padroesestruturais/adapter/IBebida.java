package cursocesar.padroesestruturais.adapter;

/**
 * @author Adler Ten�rio
 */
public interface IBebida {
	
	public void beber();

}

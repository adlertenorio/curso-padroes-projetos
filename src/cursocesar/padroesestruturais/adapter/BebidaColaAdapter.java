package cursocesar.padroesestruturais.adapter;

/**
 * @author Adler Ten�rio
 */
public class BebidaColaAdapter implements IBebida {

	private BebidaCola bebidaCola;
	
	public BebidaColaAdapter(BebidaCola bebidaCola) {
		this.bebidaCola = bebidaCola;
	}
	
	@Override
	public void beber() {
		bebidaCola.beber();
	}
}

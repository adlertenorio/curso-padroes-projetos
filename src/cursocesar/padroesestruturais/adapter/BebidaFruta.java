package cursocesar.padroesestruturais.adapter;

/**
 * @author Adler Ten�rio
 */
public class BebidaFruta {
	
	private String nome;

	public BebidaFruta(String nome) {
		this.nome = nome;
	}

	public void agitar(){
		System.out.print("Agitandar suco: "+ this.nome);
	}
	
	public void bebendo(){
		System.out.println("Bebendo...");
	}
}

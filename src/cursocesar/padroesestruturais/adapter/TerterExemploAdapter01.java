package cursocesar.padroesestruturais.adapter;
import java.util.ArrayList;


/**
 * @author Adler Ten�rio
 */
public class TerterExemploAdapter01 {
	
	public static void main(String[] args) {
		
		ArrayList<IBebida> bebidas = new ArrayList<IBebida>();

		// Bebidas Cola
		BebidaCola bebidaCola = new BebidaCola(" Coca cola ");
		BebidaCola bebidaCola1 = new BebidaCola(" Pepsi ");
		BebidaCola bebidaCola2 = new BebidaCola(" Frevo Colca ");
		
		// Adapter Bebida Cola
		BebidaColaAdapter bebidaColaAdapter = new BebidaColaAdapter(bebidaCola);
		BebidaColaAdapter bebidaColaAdapter1 = new BebidaColaAdapter(bebidaCola1);
		BebidaColaAdapter bebidaColaAdapter2= new BebidaColaAdapter(bebidaCola2);

		// Bebidas Fruta
		BebidaFruta bebidaFruta = new BebidaFruta(" Uva ");
		BebidaFruta bebidaFruta1 = new BebidaFruta(" Laranja ");
		BebidaFruta bebidaFruta2 = new BebidaFruta(" Jaca ");
		BebidaFruta bebidaFruta3 = new BebidaFruta(" Ma�� ");
		BebidaFruta bebidaFruta4 = new BebidaFruta(" Lim�o ");
		
		// Adapter Bebida Fruta
		BebidaFrutaAdapter bebidaFrutaAdapter = new BebidaFrutaAdapter(bebidaFruta);
		BebidaFrutaAdapter bebidaFrutaAdapter1 = new BebidaFrutaAdapter(bebidaFruta1);
		BebidaFrutaAdapter bebidaFrutaAdapter2 = new BebidaFrutaAdapter(bebidaFruta2);
		BebidaFrutaAdapter bebidaFrutaAdapter3 = new BebidaFrutaAdapter(bebidaFruta3);
		BebidaFrutaAdapter bebidaFrutaAdapter4 = new BebidaFrutaAdapter(bebidaFruta4);
		

		bebidas.add(bebidaFrutaAdapter4);
		bebidas.add(bebidaFrutaAdapter3);
		bebidas.add(bebidaColaAdapter);
		bebidas.add(bebidaColaAdapter2);
		bebidas.add(bebidaFrutaAdapter);
		bebidas.add(bebidaFrutaAdapter1);
		bebidas.add(bebidaFrutaAdapter2);
		bebidas.add(bebidaColaAdapter1);
		
		for (IBebida bebida : bebidas) {
			bebida.beber();
		}
	}
}

package cursocesar.padroesestruturais.bridge;
/**
 * @author Adler Ten�rio
 */
public class Carro extends AbstratcVeiculo {

	public Carro(ITrabalhador trabalhador01, ITrabalhador trabalhador02) {
		super(trabalhador01, trabalhador02);
	}

	@Override
	public void produzir() {
		System.out.print("Produzindo carro... ");
		trabalhador01.fazerTrabalho();
		trabalhador02.fazerTrabalho();
	}
}

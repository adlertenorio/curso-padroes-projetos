package cursocesar.padroesestruturais.bridge;
/**
 * @author Adler Ten�rio
 */
public class Produtor implements ITrabalhador {

	@Override
	public void fazerTrabalho() {
		System.out.println("Produzindo...");
	}
}

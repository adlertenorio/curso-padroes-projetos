package cursocesar.padroesestruturais.bridge;
/**
 * @author Adler Ten�rio
 */
public abstract class AbstratcVeiculo {

	protected ITrabalhador trabalhador01;
	protected ITrabalhador trabalhador02;

	protected AbstratcVeiculo(ITrabalhador trabalhador01, ITrabalhador trabalhador02) {
		this.trabalhador01 = trabalhador01;
		this.trabalhador02 = trabalhador02;
	}

	abstract public void produzir();
}

package cursocesar.padroesestruturais.bridge;
/**
 * @author Adler Ten�rio
 */
public interface ITrabalhador {
 
	public void fazerTrabalho();
}
 

package cursocesar.padroesestruturais.bridge;
/**
 * @author Adler Ten�rio
 */
public class TesterExemploBridge {

	public static void main(String[] args) {

		AbstratcVeiculo carro = new Carro(new Produtor(), new Montador());
		carro.produzir();
		
		AbstratcVeiculo bike = new Bike(new Produtor(), new Montador());
		bike.produzir();
	}
}
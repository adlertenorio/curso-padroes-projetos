package cursocesar.padroesestruturais.bridge;
/**
 * @author Adler Ten�rio
 */
public class Montador implements ITrabalhador {

	@Override
	public void fazerTrabalho() {
		System.out.println("Montando...");
	}
}

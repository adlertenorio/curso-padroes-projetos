package cursocesar.padroesestruturais.bridge;
/**
 * @author Adler Ten�rio
 */
public class Bike extends AbstratcVeiculo {

	public Bike(ITrabalhador trabalhador01, ITrabalhador trabalhador02) {
		super(trabalhador01, trabalhador02);
	}

	@Override
	public void produzir() {
		System.out.print("Produzindo Bike... ");
		trabalhador02.fazerTrabalho();
		trabalhador01.fazerTrabalho();
	}
}

package cursocesar.padroesestruturais.proxy;
/**
 * @author Adler Ten�rio
 */
public class TesterExemploProxy {
	
	public static void main(String[] args) {
		
		ICar carro = new CarProxy(new Motorista(16));
		carro.dirigir();

		carro = new CarProxy(new Motorista(25));
		carro.dirigir();
	}
}

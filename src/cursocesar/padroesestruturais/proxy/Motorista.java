package cursocesar.padroesestruturais.proxy;
/**
 * @author Adler Ten�rio
 */
public class Motorista {

	private int idade;

	public Motorista(int idade) {
		this.idade = idade;
	}

	public int getIdade() {
		return idade;
	}
}
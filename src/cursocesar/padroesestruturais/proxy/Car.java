package cursocesar.padroesestruturais.proxy;
/**
 * @author Adler Ten�rio
 */
public class Car implements ICar {
	
	public void dirigir() {
		System.out.println("Dirigindo carro...");
	}
}

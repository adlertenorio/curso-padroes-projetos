package cursocesar.padroesestruturais.proxy;
/**
 * @author Adler Ten�rio
 */
public interface ICar {
	
	public void dirigir();
}

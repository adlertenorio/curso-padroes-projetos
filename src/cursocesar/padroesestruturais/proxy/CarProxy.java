package cursocesar.padroesestruturais.proxy;
/**
 * @author Adler Ten�rio
 */
public class CarProxy implements ICar {

	private Motorista motorista;
	private ICar carro;

	public CarProxy(Motorista motorista) {
		this.motorista = motorista;
		this.carro = new Car();
	}

	public void dirigir() {
		if (motorista.getIdade() <= 18) {
			System.out.println("Erro: Motorista n�o pode dirigir pois � de menor.");
		} else {
			carro.dirigir();
		}
	}
}

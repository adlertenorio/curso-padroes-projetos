package cursocesar.padroescriacao.factory;

/**
 * @author Adler Ten�rio
 */
class MoedaFactory {

	public static IMoeda criarMoeda(String pais) {
		
		if (pais.equalsIgnoreCase("Brasil")) {
			return new Real();
		} else if (pais.equalsIgnoreCase("EUA")) {
			return new Dolar();
		} else if (pais.equalsIgnoreCase("Europa")) {
			return new Euro();
		}
		
		throw new IllegalArgumentException(
				"Erro: Dinheiro desconhecido.");
	}
}

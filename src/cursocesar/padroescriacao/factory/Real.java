package cursocesar.padroescriacao.factory;

/**
 * @author Adler Ten�rio
 */
public class Real implements IMoeda {
	
	@Override
	public String getSimbolo() {
		return "R$";
	}

	@Override
	public String getPresidenteResponsavelPelaMoeda() {
		return "Dilma Rousseff";
	}
}
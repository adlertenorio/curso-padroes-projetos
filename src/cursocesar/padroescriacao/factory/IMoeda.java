package cursocesar.padroescriacao.factory;

/**
 * @author Adler Ten�rio
 */
public interface IMoeda {

	String getSimbolo();
	String getPresidenteResponsavelPelaMoeda();

}

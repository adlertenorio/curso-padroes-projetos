package cursocesar.padroescriacao.factory;

/**
 * @author Adler Ten�rio
 */
public class Dolar implements IMoeda {
	
	
	@Override
	public String getSimbolo() {
		return "US$";
	}

	@Override
	public String getPresidenteResponsavelPelaMoeda() {
		return "Barack Obama";
	}
}
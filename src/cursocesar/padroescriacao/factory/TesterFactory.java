package cursocesar.padroescriacao.factory;

/**
 * # FACTORY METHOD #
 * 
 * Objetivo: Encapsular a escolha da classe concreta a ser utilizada na cria��o
 * de objetos de um determinado tipo.
 * 
 * Nesse exemplo o padr�o de projetos 'Factory Method' est� sendo criado para criar objetos com a
 * moeda do pa�s passado com par�metro, ou seja, se passar-mos o pais 'brasil' a
 * f�brica criar� um objeto da moeda desse respectivo pa�s, que no caso � REAL.
 * 
 * @author Adler Ten�rio
 */
public class TesterFactory {
	public static void main(String args[]) {

		IMoeda real = MoedaFactory.criarMoeda("brasil");
		IMoeda dolar = MoedaFactory.criarMoeda("eua");
		IMoeda europa = MoedaFactory.criarMoeda("europa");

		System.out.println("$$$ REAL $$$");
		System.out.println(real.getSimbolo());
		System.out.println(real.getPresidenteResponsavelPelaMoeda());
		System.out.println(real);

		System.out.println();

		System.out.println("$$$ DOLAR $$$");
		System.out.println(dolar.getSimbolo());
		System.out.println(dolar.getPresidenteResponsavelPelaMoeda());
		System.out.println(dolar);

		System.out.println();

		System.out.println("$$$ EURO $$$");
		System.out.println(europa.getSimbolo());
		System.out.println(europa.getPresidenteResponsavelPelaMoeda());
		System.out.println(europa);

	}
}

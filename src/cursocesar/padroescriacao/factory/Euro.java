package cursocesar.padroescriacao.factory;

/**
 * @author Adler Ten�rio
 */
public class Euro implements IMoeda {
	
	@Override
	public String getSimbolo() {
		return "EU";
	}

	@Override
	public String getPresidenteResponsavelPelaMoeda() {
		return "Uni�o Europeia";
	}
}

package cursocesar.padroescriacao.abstractfactory;

/**
 * @author Adler Ten�rio
 */
public class RetaMotif extends AbstractReta {

	@Override
	public void desenhar() {
		System.out.println("Desenhar Reta Motifi");
	}
}

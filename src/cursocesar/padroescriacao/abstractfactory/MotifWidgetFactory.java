package cursocesar.padroescriacao.abstractfactory;

/**
 * @author Adler Ten�rio
 */
public class MotifWidgetFactory extends AbstractWidgetFactory {
    
	public AbstractBotao criarBotao()  {
         return new BotaoMotif();
     }
     
     public AbstractReta criarReta()  {
         return new RetaMotif();
     }
 }
 
package cursocesar.padroescriacao.abstractfactory;

/**
 * # ABSTRACT FACTORY #
 * 
 * Objetivo: Encapsular a escolha das classes concretas a serem utilizadas na
 * cria��o dos objetos de diversas fam�lias.
 * 
 * Nesse exemplo o padr�o de projetos 'Abstract Factory' est� sendo usado para criar 2
 * fabricas, a primeira � f�brica trata-se da frabrica MOTIFI e a mesma �
 * respons�vel por criar objetos desse referido tipo, a segunda f�brica � a QT
 * que tamb�m � respons�vel por criar objetos do seu tipo.
 * 
 * @author Adler Ten�rio
 */
public class TesterAbstractFactory {

	public static void main(String[] args) {

		AbstractWidgetFactory factoryMotifi = AbstractWidgetFactory
				.obterFactory(Valores.MOTIFI);

		AbstractBotao botaoMotifi = factoryMotifi.criarBotao();
		AbstractReta retaMotifi = factoryMotifi.criarReta();
		
		System.out.println("#### Campos Fabrica Motifi ####");

		retaMotifi.desenhar();
		botaoMotifi.desenhar();

		System.out.println();
		System.out.println("#### Campos Fabrica QT ####");

		AbstractWidgetFactory factoryQT = AbstractWidgetFactory
				.obterFactory(Valores.QT);

		AbstractBotao botaoQT = factoryQT.criarBotao();
		AbstractReta retaQT = factoryQT.criarReta();

		botaoQT.desenhar();
		retaQT.desenhar();

	}
}
package cursocesar.padroescriacao.abstractfactory;

/**
 * @author Adler Ten�rio
 */
public abstract class AbstractReta {

	public abstract void desenhar();
	
}

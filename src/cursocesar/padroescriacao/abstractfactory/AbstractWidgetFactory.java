package cursocesar.padroescriacao.abstractfactory;

/**
 * @author Adler Ten�rio
 */
public abstract class AbstractWidgetFactory {
	
	public static AbstractWidgetFactory obterFactory(int valor) {

		if (Valores.MOTIFI == valor) {
			return new MotifWidgetFactory();
		} else {
			return new QTWidgetFactory();
		}
	}

	public abstract AbstractBotao criarBotao();
	public abstract AbstractReta criarReta();
}

package cursocesar.padroescriacao.abstractfactory;

/**
 * @author Adler Ten�rio
 */
public abstract class AbstractBotao {
     
	public abstract void desenhar();
 }
 
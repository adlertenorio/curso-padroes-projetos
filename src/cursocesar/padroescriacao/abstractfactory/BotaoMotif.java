package cursocesar.padroescriacao.abstractfactory;

/**
 * @author Adler Ten�rio
 */
public class BotaoMotif extends AbstractBotao {
     
	public void desenhar()  {
        System.out.println("Desenhar Botao Motif");
     }
 }
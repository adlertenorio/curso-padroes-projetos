package cursocesar.padroescriacao.abstractfactory;

/**
 * @author Adler Ten�rio
 */
public class QTWidgetFactory extends AbstractWidgetFactory {
   
	public AbstractBotao criarBotao()  {
         return new BotaoQT();
     }
     
     public AbstractReta criarReta()  {
         return new RetaQT();
     }
  }
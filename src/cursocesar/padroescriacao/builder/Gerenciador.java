package cursocesar.padroescriacao.builder;

/**
 * @author Adler Ten�rio
 */
class Gerenciador {
	
	private AbstractPizzaBuilder pizzaBuilder;

	public void setPizzaBuilder(AbstractPizzaBuilder pizzaBuilder) {
		this.pizzaBuilder = pizzaBuilder;
	}

	public Pizza getPizza() {
		return pizzaBuilder.getPizza();
	}

	public void criartPizza() {
		this.pizzaBuilder.criarPizza();
		this.pizzaBuilder.buildMolho();
		this.pizzaBuilder.buildCobertura();
	}
}

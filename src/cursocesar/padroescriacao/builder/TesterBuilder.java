package cursocesar.padroescriacao.builder;

/**
 * # BUILDER #
 * 
 * Objetivo: Separar o processo de constru��o de um objeto de sua representa��o
 * e permitir a sua cria��o passo-a-passo. Diferentes tipos de objetos podem ser
 * criados com implementa��es distintas de cada passo.
 * 
 * Nesse exemplo o padr�o de projetos 'Builder' est� sendo respons�vel por
 * administrar a cria��o de dois objetos pizza, primeiro recebendo (gerenciador.setPizzaBuilder(havaianaPizzaBuilder)) a forma
 * (sabor) que o objeto vai se tornar, depois criando-o (gerenciador.criartPizza()) e depois exibindo os atributos do mesmo.
 * 
 * @author Adler Ten�rio
 */
public class TesterBuilder {

	public static void main(String[] args) {

		Gerenciador gerenciador = new Gerenciador();

		gerenciador.setPizzaBuilder(new HavaianaPizzaBuilder());
		gerenciador.criartPizza();
		Pizza pizza = gerenciador.getPizza();
		System.out.println("PIZZA HAVAIANA: " + pizza.getCobertura());

		gerenciador.setPizzaBuilder(new ApimentadaPizzaBuilder());
		gerenciador.criartPizza();
		pizza = gerenciador.getPizza();
		System.out.println("PIZZA APIMENTADA: " + pizza.getCobertura());

	}
}
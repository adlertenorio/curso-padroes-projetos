package cursocesar.padroescriacao.builder;

/**
 * @author Adler Ten�rio
 */
class ApimentadaPizzaBuilder extends AbstractPizzaBuilder {

	public ApimentadaPizzaBuilder(){
		super();
	}
	
	public void buildMolho() {
		pizza.setMolho("Pimenta Malagueta");
	}

	public void buildCobertura() {
		pizza.setCobertura("Peperoni, pimenta, piment�o");
	}
}

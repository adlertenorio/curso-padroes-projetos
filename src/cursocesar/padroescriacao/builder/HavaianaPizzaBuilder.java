package cursocesar.padroescriacao.builder;

/**
 * @author Adler Ten�rio
 */
class HavaianaPizzaBuilder extends AbstractPizzaBuilder {
	
	public HavaianaPizzaBuilder() {
		super();
	}
	
	public void buildMolho() {
		pizza.setMolho("Molho havaiano");
	}

	public void buildCobertura() {
		pizza.setCobertura("Abacaxi e presunto");
	}
}

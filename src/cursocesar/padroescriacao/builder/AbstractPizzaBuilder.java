package cursocesar.padroescriacao.builder;

/**
 * @author Adler Ten�rio
 */
abstract class AbstractPizzaBuilder {
	
	protected Pizza pizza;
	
	public AbstractPizzaBuilder() {
		
	}

	public void criarPizza() {
		pizza = new Pizza();
	}

	public Pizza getPizza() {
		return pizza;
	}
	
	public abstract void buildMolho();
	public abstract void buildCobertura();

}

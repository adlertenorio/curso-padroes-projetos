package cursocesar.padroescriacao.builder;

/**
 * @author Adler Ten�rio
 */
public class Pizza {

	private String molho = "";
	private String cobertura = "";

	public String getMolho() {
		return molho;
	}

	public void setMolho(String molho) {
		this.molho = molho;
	}

	public String getCobertura() {
		return cobertura;
	}

	public void setCobertura(String cobertura) {
		this.cobertura = cobertura;
	}
}

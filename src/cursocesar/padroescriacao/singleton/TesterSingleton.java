package cursocesar.padroescriacao.singleton;

/**
 * # SINGLETON #
 * 
 * Objetivo: Permitir a cria��o de uma �nica inst�ncia de uma classe e fornecer
 * um modo para recuper�-la.
 * 
 * Nesse exemplo o padr�o de projetos 'Singleton' � respons�vel por recuperar a
 * mesma inst�ncia 2 vezes e provar que a inst�ncia retornada � igual. 
 * A referida inst�ncia tem um m�todo 'exibirHora' que imprime no console a data e
 * a hora que a inst�ncia foi criada, com isso prova-se que � a mesma inst�ncia.
 * 
 * @author Adler Ten�rio
 */
public class TesterSingleton {

	public static void main(String[] args) {

		SingletonDataHora instancia1 = SingletonDataHora.obterInstancia();

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		SingletonDataHora instancia2 = SingletonDataHora.obterInstancia();

		System.out.println("Primeira inst�ncia HashCode: " + instancia1);
		instancia1.exibirHora();
		System.out.println("Segunda inst�ncia HashCode: " + instancia2);
		instancia2.exibirHora();
	}
}

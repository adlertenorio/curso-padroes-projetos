package cursocesar.padroescriacao.singleton;

import java.util.Date;

/**
 * @author Adler Ten�rio
 */
public class SingletonDataHora {

	private static SingletonDataHora instance;
	private Date data;

	private SingletonDataHora() {
		this.data = new Date(System.currentTimeMillis());
	}

	public static SingletonDataHora obterInstancia() {
		if (instance == null) {
			instance = new SingletonDataHora();
		}
		return instance;
	}

	public void exibirHora() {
		System.out.println(this.data.toGMTString());
	}
}

/**
 * @author Adler Ten�rio
 */
package cursocesar.combinacaopadroes.command_factory_singleton.factory;


import cursocesar.combinacaopadroes.command_factory_singleton.negocio.IArma;
import cursocesar.combinacaopadroes.command_factory_singleton.negocio.Metralhadora;
import cursocesar.combinacaopadroes.command_factory_singleton.negocio.Pistola;
import cursocesar.combinacaopadroes.command_factory_singleton.negocio.Rifle;
import cursocesar.combinacaopadroes.command_factory_singleton.negocio.TipoArmaEnum;



/**
 * Classe utilit�ria com m�todos est�ticos 
 */
public class ArmaFactory {

	/**
	 * Construtor privado para evitar que seja criado inst�ncias da classe, pois
	 * a ideia da mesma � ser uma classe utilit�ria com m�todos est�ticos.
	 */
	private ArmaFactory() {
	}

	public static IArma criarArma(TipoArmaEnum tipo) {

		switch (tipo) {

		case METRALHADORA:
			return new Metralhadora(TipoArmaEnum.METRALHADORA);

		case PISTOLA:
			return new Pistola(TipoArmaEnum.PISTOLA);

		case RIFLE:
			return new Rifle(TipoArmaEnum.RIFLE);

		default:
			throw new IllegalArgumentException("Erro: Arma n�o encontrada.");
		}
	}
}

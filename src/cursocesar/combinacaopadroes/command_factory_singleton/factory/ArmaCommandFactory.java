/**
 * @author Adler Ten�rio
 */
package cursocesar.combinacaopadroes.command_factory_singleton.factory;

import java.util.Date;

import cursocesar.combinacaopadroes.command_adapter_singleton.util.IArma;
import cursocesar.combinacaopadroes.command_factory_singleton.command.AtirarCommand;
import cursocesar.combinacaopadroes.command_factory_singleton.command.CarregarCommand;
import cursocesar.combinacaopadroes.command_factory_singleton.command.ICommand;
import cursocesar.combinacaopadroes.command_factory_singleton.command.MirarCommand;




public class ArmaCommandFactory {
	
	/**
	 * Construtor privado para evitar que seja criado inst�ncias da classe, pois
	 * a ideia da mesma � ser uma classe utilit�ria com m�todos est�ticos.
	 */
	private ArmaCommandFactory() {
	}
	
	public static ICommand criarAtirarCommand(IArma arma) {
		AtirarCommand atirarCommand = new AtirarCommand(arma);
		atirarCommand.setDataCriacao(new Date());
		
		return atirarCommand ;
	}
	
	public static ICommand criarMirarCommand(IArma arma) {
		MirarCommand mirarCommand = new MirarCommand(arma);
		mirarCommand.setDataCriacao(new Date());
		
		return mirarCommand;
	}
	
	public static ICommand criarCarregarCommand(IArma arma) {
		CarregarCommand carregarCommand = new CarregarCommand(arma);
		carregarCommand.setDataCriacao(new Date());
		
		return carregarCommand;
	}
}

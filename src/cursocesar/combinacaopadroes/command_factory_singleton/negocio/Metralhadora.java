package cursocesar.combinacaopadroes.command_factory_singleton.negocio;


/**
 * @author Adler Ten�rio
 */
public class Metralhadora implements IArma {
	
	private TipoArmaEnum tipo;
	
	public Metralhadora(TipoArmaEnum tipo) {
		this.tipo = tipo;
	}

	@Override
	public void mirar() {
		System.out.println("Mirando metralhadora...");
	}
	
	@Override
	public void atirar() {
		System.out.println("Atirando metralhadora...");
	}
	
	@Override
    public void carregar() {
    	System.out.println("Carregando metralhadora...");	
    }

	public TipoArmaEnum getTipo() {
		return tipo;
	}

	public void setTipo(TipoArmaEnum tipo) {
		this.tipo = tipo;
	}
}

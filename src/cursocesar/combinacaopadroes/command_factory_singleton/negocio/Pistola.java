

/**
 * @author Adler Ten�rio
 */
package cursocesar.combinacaopadroes.command_factory_singleton.negocio;

public class Pistola implements IArma {

	private TipoArmaEnum tipo;

	public Pistola(TipoArmaEnum tipo) {
		this.tipo = tipo;
	}

	@Override
	public void mirar() {
		System.out.println("Mirando pistola...");
	}

	@Override
	public void atirar() {
		System.out.println("Atirando pistola...");
	}

	@Override
	public void carregar() {
		System.out.println("Carregando pistola...");
	}

	public TipoArmaEnum getTipo() {
		return tipo;
	}

	public void setTipo(TipoArmaEnum tipo) {
		this.tipo = tipo;
	}
}

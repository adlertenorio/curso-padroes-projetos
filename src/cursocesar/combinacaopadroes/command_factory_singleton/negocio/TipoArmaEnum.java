package cursocesar.combinacaopadroes.command_factory_singleton.negocio;

/**
 * @author Adler Ten�rio
 */
public enum TipoArmaEnum {

	METRALHADORA, PISTOLA, RIFLE;
}

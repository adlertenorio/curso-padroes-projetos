/**
 * @author Adler Ten�rio
 */
package cursocesar.combinacaopadroes.command_factory_singleton.negocio;

public class Rifle implements IArma {

	private TipoArmaEnum tipo;

	public Rifle(TipoArmaEnum tipo) {
		this.tipo = tipo;
	}

	@Override
	public void mirar() {
		System.out.println("Mirando rifle...");
	}

	@Override
	public void atirar() {
		System.out.println("Atirando rifle...");
	}

	@Override
	public void carregar() {
		System.out.println("Carregando rifle...");
	}

	public TipoArmaEnum getTipo() {
		return tipo;
	}

	public void setTipo(TipoArmaEnum tipo) {
		this.tipo = tipo;
	}
}

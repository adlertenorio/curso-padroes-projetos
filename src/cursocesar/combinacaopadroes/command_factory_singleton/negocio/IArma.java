package cursocesar.combinacaopadroes.command_factory_singleton.negocio;

/**
 * @author Adler Ten�rio
 */
public interface IArma {
	
	public void mirar();
	public void atirar();
	public void carregar();
}

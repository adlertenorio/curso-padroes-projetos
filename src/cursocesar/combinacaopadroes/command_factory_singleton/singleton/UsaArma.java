package cursocesar.combinacaopadroes.command_factory_singleton.singleton;

import java.util.ArrayList;
import java.util.List;

import cursocesar.combinacaopadroes.command_adapter_singleton.command.ICommand;


/**
 * @author Adler Ten�rio
 */
public class UsaArma {

	private List<ICommand> historicos = new ArrayList<ICommand>();
	private static UsaArma instance;
	
	private UsaArma() {
		
	}
	
	public static UsaArma getInstance() {
		if (instance == null) {
			instance = new UsaArma();
		}
		
		return instance;
	}
	
	public void adicionaEExecutaComando(ICommand comando) {
		this.historicos.add(comando);
		comando.execute();
	}
}

package cursocesar.combinacaopadroes.command_factory_singleton.command;

/**
 * @author Adler Ten�rio
 */
public interface ICommand {
	
	public void execute();

}

package cursocesar.combinacaopadroes.command_factory_singleton.command;

import java.util.Date;

import cursocesar.combinacaopadroes.command_adapter_singleton.util.IArma;


/**
 * @author Adler Ten�rio
 */
public class CarregarCommand implements ICommand {

	private IArma arma;
	private Date dataCriacao;

	public CarregarCommand(IArma arma) {
		super();
		this.arma = arma;
	}

	@Override
	public void execute() {
		this.arma.carregar();
	}
	
	public Date getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}
}

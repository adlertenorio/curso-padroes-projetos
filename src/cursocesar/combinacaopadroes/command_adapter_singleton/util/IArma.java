package cursocesar.combinacaopadroes.command_adapter_singleton.util;


/**
 * @author Adler Ten�rio
 */
public interface IArma {

	public void mirar();
	public void atirar();
	public void carregar();

}

package cursocesar.combinacaopadroes.command_adapter_singleton.tester;

import cursocesar.combinacaopadroes.command_adapter_singleton.adapter.BazucaAdapter;
import cursocesar.combinacaopadroes.command_adapter_singleton.adapter.Metraladora;
import cursocesar.combinacaopadroes.command_adapter_singleton.command.AtirarCommand;
import cursocesar.combinacaopadroes.command_adapter_singleton.command.CarregarCommand;
import cursocesar.combinacaopadroes.command_adapter_singleton.command.ICommand;
import cursocesar.combinacaopadroes.command_adapter_singleton.command.MirarCommand;
import cursocesar.combinacaopadroes.command_adapter_singleton.singleton.UsaArma;
import cursocesar.combinacaopadroes.command_adapter_singleton.util.IArma;

/**
 * @author Adler Ten�rio
 */
public class TesterCommandAdapterSingleton {

	public static void main(String[] args) {

		IArma metraladora = new Metraladora();

		// Command
		ICommand atirarCommandMetralhadora = new AtirarCommand(metraladora);
		ICommand mirarCommandMetralhadora = new MirarCommand(metraladora);
		ICommand carregarCommandMetralhadora = new CarregarCommand(metraladora);


		// Adapter
		/**
		 * Digamos que essa classe foi Bazuca adicionada atrav�s de um jar
		 * externo e n�s n�o podemos alterar o c�digo fonte dessa classe,
		 * podendo ver somente o java doc e ver o que os m�todos referentes a
		 * essa classe fazem, por isso a cria��o da classe BazucaAdapter.
		 */
		IArma bazucaAdapter = new BazucaAdapter();
		
		// Command
		ICommand atirarCommandBazuca = new AtirarCommand(bazucaAdapter);
		ICommand mirarCommandBazuca = new MirarCommand(bazucaAdapter);
		ICommand carregarCommandBazuca = new CarregarCommand(bazucaAdapter);

		// Singleton
		UsaArma usaArma = UsaArma.getInstance();

		usaArma.adicionaEExecutaComando(atirarCommandMetralhadora);
		usaArma.adicionaEExecutaComando(mirarCommandMetralhadora);
		usaArma.adicionaEExecutaComando(carregarCommandMetralhadora);
		
		usaArma.adicionaEExecutaComando(atirarCommandBazuca);
		usaArma.adicionaEExecutaComando(mirarCommandBazuca);
		usaArma.adicionaEExecutaComando(carregarCommandBazuca);

	}
}

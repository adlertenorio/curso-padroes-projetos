package cursocesar.combinacaopadroes.command_adapter_singleton.command;

/**
 * @author Adler Ten�rio
 */
public interface ICommand {
	
	public void execute();

}

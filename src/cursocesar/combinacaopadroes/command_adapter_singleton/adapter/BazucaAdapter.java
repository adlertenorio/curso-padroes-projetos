package cursocesar.combinacaopadroes.command_adapter_singleton.adapter;

import cursocesar.combinacaopadroes.command_adapter_singleton.util.IArma;


/**
 * @author Adler Ten�rio
 */
public class BazucaAdapter extends Bazuca implements IArma {

	@Override
	public void mirar() {
		super.mirarAltoAlcance();
	}

	@Override
	public void atirar() {
		super.atirarMissil();
	}

	@Override
	public void carregar() {
		super.carregarMissil();
	}
}

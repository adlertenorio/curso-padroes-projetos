package cursocesar.combinacaopadroes.command_adapter_singleton.adapter;


/**
 * @author Adler Ten�rio
 * 
 *         Digamos que essa classe foi adicionada atravpes de um jar externo e
 *         n�s n�o podemos alterar o c�digo fonte dessa classe, podendo ver somente o java doc e ver o que os m�todos referentes a essa classe fazem.
 */
public class Bazuca {

	public void mirarAltoAlcance() {
		System.out.println("Mirando alto alcance Bazuca ");
	}

	public void atirarMissil() {
		System.out.println("Atirando missil Bazuca ");

	}

	public void carregarMissil() {
		System.out.println("Carregando missil Bazuca ");
	}
}

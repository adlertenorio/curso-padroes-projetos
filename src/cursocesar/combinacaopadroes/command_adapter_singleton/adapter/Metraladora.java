package cursocesar.combinacaopadroes.command_adapter_singleton.adapter;

import cursocesar.combinacaopadroes.command_adapter_singleton.util.IArma;


/**
 * @author Adler Ten�rio
 */
public class Metraladora implements IArma {

	public void mirar() {
		System.out.println("Mirando Metraladora ");
	}

	public void atirar() {
		System.out.println("Atirando Metraladora");
	}

	public void carregar() {
		System.out.println("Carregando Metraladora");
	}
}

package cursocesar.combinacaopadroes.facade_singleton.facade;

import cursocesar.combinacaopadroes.facade_singleton.negocio.conta.Conta;
import cursocesar.combinacaopadroes.facade_singleton.negocio.conta.ContaController;
import cursocesar.combinacaopadroes.facade_singleton.negocio.usuario.Usuario;
import cursocesar.combinacaopadroes.facade_singleton.negocio.usuario.UsuarioController;

/**
 * @author Adler Ten�rio
 */
public class Fachada {
	
	private static Fachada fachada;
	
	private UsuarioController usuarioController;
	private ContaController contaController;
	
	private Fachada() {
		this.usuarioController = new UsuarioController();
		this.contaController = new ContaController();
	}
	
	public static Fachada obterInstancia() {
		if (fachada == null) {
			fachada = new Fachada();
		}
		return fachada;
	}
	
	// Usu�rio
	public void incluirUsuario(Usuario usuario) {
		usuarioController.incluir(usuario);
		System.out.println("Usu�rio Inclu�do com Sucesso !");
	}
	
	public void alterarUsuario(Usuario usuario) {
		usuarioController.alterar(usuario);
		System.out.println("Usu�rio Alterado com Sucesso !");
	}
	
	public void excluirUsuario(Usuario usuario) {
		usuarioController.excluir(usuario);
		System.out.println("Usu�rio Exclu�do com Sucesso !");
	}
	
	public Usuario buscarUsuario(Integer id) {
		return usuarioController.buscar(id);
	}
	
	// Conta
	public void incluirConta(Conta conta) {
		contaController.incluir(conta);
		System.out.println("Conta Inclu�da com Sucesso !");
	}
	
	public void alterarConta(Conta conta) {
		contaController.alterar(conta);
		System.out.println("Conta Alterado com Sucesso !");
	}
	
	public void excluirConta(Conta conta) {
		contaController.excluir(conta);
		System.out.println("Conta Exclu�do com Sucesso !");
	}
	
	public Conta buscarConta(Integer id) {
		return contaController.buscar(id);
	}
}

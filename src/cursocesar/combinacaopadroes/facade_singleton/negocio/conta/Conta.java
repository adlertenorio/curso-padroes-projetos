package cursocesar.combinacaopadroes.facade_singleton.negocio.conta;

import cursocesar.combinacaopadroes.facade_singleton.negocio.usuario.Usuario;


/**
 * @author Adler Ten�rio
 */
public class Conta {
	
	private Integer id;
	private Usuario usuario;
	private String agencia;
	private Integer numero;
	

	public Conta(Usuario usuario, String agencia, Integer numero) {
		super();
		this.usuario = usuario;
		this.agencia = agencia;
		this.numero = numero;
	}
	
	public Integer getId() {
		return id;
	}
	
	public Usuario getUsuario() {
		return usuario;
	}
	
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public String getAgencia() {
		return agencia;
	}
	
	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}
	
	public Integer getNumero() {
		return numero;
	}
	
	public void setNumero(Integer numero) {
		this.numero = numero;
	}
}

package cursocesar.combinacaopadroes.facade_singleton.negocio.conta;
/**
 * @author Adler Ten�rio
 */
public class ContaController {
	
	private ContaDAO contaDAO;

	public ContaController() {
		this.contaDAO = new ContaDAO();
	}
	
	public void incluir(Conta conta) {
		contaDAO.incluir(conta);
	}
	
	public void alterar(Conta conta) {
		contaDAO.alterar(conta);
	}
	
	public void excluir(Conta conta) {
		contaDAO.excluir(conta);
	}
	
	public Conta buscar(Integer id) {
		return contaDAO.buscar(id);
	}
}

package cursocesar.combinacaopadroes.facade_singleton.negocio.conta;

import cursocesar.combinacaopadroes.facade_singleton.negocio.usuario.Usuario;


/**
 * @author Adler Ten�rio
 */
public class ContaDAO {
	
	public ContaDAO() {
		super();
	}

	public void incluir(Conta conta) {
		System.out.println("Save conta");
	}
	
	public void alterar(Conta conta) {
		System.out.println("Update conta");
	}
	
	public void excluir(Conta conta) {
		System.out.println("Delete conta");
	}
	
	public Conta buscar(Integer id) {
		return new Conta(new Usuario("Jose da Silva", "11111111"), "0009-x", 1234);
	}
}

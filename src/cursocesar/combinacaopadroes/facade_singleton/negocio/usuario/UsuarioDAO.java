package cursocesar.combinacaopadroes.facade_singleton.negocio.usuario;

/**
 * @author Adler Ten�rio
 */
public class UsuarioDAO {
	
	public UsuarioDAO() {
		super();
	}

	public void incluir(Usuario usuario) {
		System.out.println("Save usuario");
	}
	
	public void alterar(Usuario usuario) {
		System.out.println("Update usuario");
	}
	
	public void excluir(Usuario usuario) {
		System.out.println("Delete usuario");
	}
	
	public Usuario buscar(Integer id) {
		return new Usuario("Usu�rio do banco", "111111111");
	}
}

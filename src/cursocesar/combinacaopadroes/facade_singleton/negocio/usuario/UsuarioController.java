package cursocesar.combinacaopadroes.facade_singleton.negocio.usuario;

/**
 * @author Adler Ten�rio
 */
public class UsuarioController {

	private UsuarioDAO usuarioDAO;

	public UsuarioController() {
		this.usuarioDAO = new UsuarioDAO();
	}
	
	
	public void incluir(Usuario usuario) {
		usuarioDAO.incluir(usuario);
	}
	
	public void alterar(Usuario usuario) {
		usuarioDAO.alterar(usuario);
	}
	
	public void excluir(Usuario usuario) {
		usuarioDAO.excluir(usuario);
	}
	
	public Usuario buscar(Integer id) {
		return usuarioDAO.buscar(id);
	}
}

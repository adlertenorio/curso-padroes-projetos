package cursocesar.combinacaopadroes.facade_singleton.negocio.usuario;

/**
 * @author Adler Ten�rio
 */
public class Usuario {
	
	private Integer id;
	private String nome;
	private String cpf;
	
	
	public Usuario(String nome, String cpf) {
		super();
		this.nome = nome;
		this.cpf = cpf;
	}
	
	public Integer getId() {
		return id;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getCpf() {
		return cpf;
	}
	
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	
}

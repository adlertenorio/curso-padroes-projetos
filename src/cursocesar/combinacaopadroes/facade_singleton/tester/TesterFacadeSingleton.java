package cursocesar.combinacaopadroes.facade_singleton.tester;

import cursocesar.combinacaopadroes.facade_singleton.facade.Fachada;
import cursocesar.combinacaopadroes.facade_singleton.negocio.conta.Conta;
import cursocesar.combinacaopadroes.facade_singleton.negocio.usuario.Usuario;

/** 
 * @author Adler Ten�rio
 */
public class TesterFacadeSingleton {

	public static void main(String[] args) {

		// Obtendo a fachada atrav�s de um m�todo singleton
		Fachada fachada = Fachada.obterInstancia();

		/**
		 *  Opera��es Usu�rio
		 */
		Usuario usuarioAdler = new Usuario("Adler Ten�rio", "123456");

		fachada.incluirUsuario(usuarioAdler);

		usuarioAdler.setNome("Pedro");
		fachada.alterarUsuario(usuarioAdler);

		Usuario usuario = fachada.buscarUsuario(usuarioAdler.getId());
		fachada.excluirUsuario(usuario);

		/**
		 *  Opera��es Conta
		 */
		Conta conta = new Conta(usuarioAdler, "00001-X", 785424);
		
		fachada.incluirConta(conta);
		Conta contaConsultada = fachada.buscarConta(2);
		
		contaConsultada.setNumero(888888);
		fachada.alterarConta(contaConsultada);
		
		fachada.excluirConta(contaConsultada);

	}
}

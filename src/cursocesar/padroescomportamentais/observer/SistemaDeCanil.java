package cursocesar.padroescomportamentais.observer;
/**
 * @author Adler Ten�rio
 */
public class SistemaDeCanil implements ISistemaSeguranca {

	@Override
	public void soarAlarme() {
		System.out.println("Evacuando canil...");

	}

}

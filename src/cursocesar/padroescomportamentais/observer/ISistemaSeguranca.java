package cursocesar.padroescomportamentais.observer;
/**
 * @author Adler Ten�rio
 */
public interface ISistemaSeguranca {

	public void soarAlarme();
	
}

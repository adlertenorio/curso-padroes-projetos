package cursocesar.padroescomportamentais.observer;
/**
 * @author Adler Ten�rio
 */
public class SistemaDeEvacuacao implements ISistemaSeguranca {

	@Override
	public void soarAlarme() {
		System.out.println("Evacuando sistema...");

	}

}

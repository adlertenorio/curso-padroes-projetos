package cursocesar.padroescomportamentais.observer;
/**
 * @author Adler Ten�rio
 */
public class TesterExemploObserver {
	
	public static void main(String[] args) {

		Observer observer = new Observer();
		
		observer.addSistema(new SistemaDeCanil());
		observer.addSistema(new SistemaDeEvacuacao());
		
		observer.soarAlarme();
		
	}
}

package cursocesar.padroescomportamentais.observer;

import java.util.ArrayList;

/**
 * @author Adler Ten�rio
 */
public class Observer {

	private ArrayList<ISistemaSeguranca> sistemas = new ArrayList<ISistemaSeguranca>();
	
	public Observer(){
		this.sistemas = new ArrayList<ISistemaSeguranca>();
	}
	
	public void addSistema(ISistemaSeguranca sistema){
	
		if(this.sistemas == null){
			this.sistemas =  new ArrayList<ISistemaSeguranca>();
		}
		
		this.sistemas.add(sistema);
	}
	
	public void soarAlarme(){
		for (int i = 0; i < this.sistemas.size(); i++) {
			this.sistemas.get(i).soarAlarme();
		}
	}
}

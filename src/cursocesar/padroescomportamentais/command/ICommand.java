package cursocesar.padroescomportamentais.command;

/**
 * @author Adler Ten�rio
 */
public interface ICommand {
   
	public void execute();
}
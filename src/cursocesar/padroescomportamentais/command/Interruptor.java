package cursocesar.padroescomportamentais.command;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Adler Ten�rio
 */
public class Interruptor {

   private List<ICommand> historicos = new ArrayList<ICommand>();
 
   public void adicionaEExecutaComandos(ICommand comando) {
      this.historicos.add(comando);
      comando.execute();        
   }
}
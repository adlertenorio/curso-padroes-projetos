package cursocesar.padroescomportamentais.command;

/**
 * @author Adler Ten�rio
 */
public class Lampada {
   
   public void ligar() {
      System.out.println("A lampada est� ligada !");
   }
 
   public void desligar() {
      System.out.println("A lampada esta desligada! ");
   }
}
package cursocesar.padroescomportamentais.command;

/**
 * @author Adler Ten�rio
 */
public class DesligarCommand implements ICommand {

	private Lampada lampada;

	public DesligarCommand(Lampada lampada) {
		this.lampada = lampada;
	}

	@Override
	public void execute() {
		this.lampada.desligar();
	}
}

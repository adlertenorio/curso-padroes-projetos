package cursocesar.padroescomportamentais.command;

/**
 * @author Adler Ten�rio
 */
public class LigarCommand implements ICommand {
   
	private Lampada lampada;
 
   public LigarCommand(Lampada lampada) {
      this.lampada = lampada;
   }
 
   public void execute(){
      lampada.ligar();
   }
}
 
package cursocesar.padroescomportamentais.command;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TesterExemploCommand {

	public static void main(String[] args) throws IOException {

		Lampada lampada = new Lampada();
		ICommand ligarCommand = new LigarCommand(lampada);
		ICommand desligarCommand = new DesligarCommand(lampada);

		Interruptor interruptor = new Interruptor();

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String sample;

		do {
			System.out
					.print("Digite ON para ligar, OFF para desligar, SAIR para sair: ");
			sample = br.readLine();
			try {
				if (sample.equalsIgnoreCase("ON")) {
					interruptor.adicionaEExecutaComandos(ligarCommand);
				} else if (sample.equalsIgnoreCase("OFF")) {
					interruptor.adicionaEExecutaComandos(desligarCommand);
				} else {
					if (sample.equalsIgnoreCase("Sair")) {
						System.out.println("Vc est� saindo...");
					} else {
						System.out
								.println("Vc precisa informar \"ON\" ou \"OFF\"");
					}
				}
			} catch (Exception e) {
				System.out.println("Error!");
			}
		} while (!sample.equalsIgnoreCase("sair"));
	}
}
package cursocesar.padroescomportamentais.iterator;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.NoSuchElementException;

/**
 * @author Adler Ten�rio
 */
public class IntSet {
	
	private Hashtable hashtable = new Hashtable();

	public static class Iterator {
		
		
		private IntSet intSet;
		private Enumeration enumeration;
		private Integer current;

		public Iterator(IntSet in) {
			this.intSet = in;
		}

		public void first() {
			this.enumeration = this.intSet.hashtable.keys();
			next();
		}

		public boolean isDone() {
			return current == null;
		}

		public int currentItem() {
			return current.intValue();
		}

		public void next() {
			try {
				current = (Integer) enumeration.nextElement();
			} catch (NoSuchElementException e) {
				current = null;
			}
		}
	}

	public void add(int in) {
		hashtable.put(new Integer(in), "null");
	}

	public boolean contem(int i) {
		return hashtable.containsKey(new Integer(i));
	}

	public Hashtable getHashtable() {
		return hashtable;
	}

	public Iterator criarIterator() {
		return new Iterator(this);
	}
}

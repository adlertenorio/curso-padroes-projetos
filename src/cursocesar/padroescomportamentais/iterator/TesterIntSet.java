package cursocesar.padroescomportamentais.iterator;

import java.util.Enumeration;

/**
 * @author Adler Ten�rio
 */
public class TesterIntSet {

	public static void main(String[] args) {

		IntSet conjunto = new IntSet();

		// Adicionando
		for (int i = 2; i < 10; i += 2) {
			conjunto.add(i);
		}

		// Verificando se foi adicionado
		for (int i = 1; i < 9; i++) {
			System.out.print(i + "-" + conjunto.contem(i) + "  ");
		}
		System.out.println("\n-------------------------------------------");

		// Recuperando o iterator
		IntSet.Iterator itiretor1 = conjunto.criarIterator();
		IntSet.Iterator iterator2 = conjunto.criarIterator();

		// Usamos First, isDone,Next,CurrentItem
		System.out.print("\nIterator:    ");
		for (itiretor1.first(), iterator2.first(); !itiretor1.isDone(); itiretor1.next(), iterator2.next()) {
			System.out.print(itiretor1.currentItem() + " " + iterator2.currentItem() + "  ");
		}
		
		System.out.println("\n-------------------------------------------");
		// Em java atraves de ENUM tb temos o conceito de iterator.
		System.out.print("\nJAVA - Enumeration: ");

		for (Enumeration e = conjunto.getHashtable().keys(); e.hasMoreElements();) {
			System.out.print(e.nextElement() + "  ");
		}
	}
}

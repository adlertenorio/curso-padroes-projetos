package cursocesar.padroescomportamentais.strategy;

/**
 * @author Adler Ten�rio
 */
public class BancoCentralInterior extends AbstractRegistrarStrategyInterior{
	
	@Override
	void criarCadastro() {
		System.out.println("Criando cadastro Banco Central do Interior");
	}

	@Override
	void fazerAlgo() {
		System.out.println("Fazendo algo Banco Central do Interior");
		
	}

	@Override
	void adicionar() {
		System.out.println("Adicionando Banco Central do Interior");
	}
}

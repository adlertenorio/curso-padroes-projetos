package cursocesar.padroescomportamentais.strategy;

/**
 * @author Adler Ten�rio
 */
public class BancoCentral extends AbstractRegistrarStrategy{

	@Override
	void criarCadastro() {
		System.out.println("Criando cadastro Banco Central");
	}

	@Override
	void fazerAlgo() {
		System.out.println("Fazendo algo Banco Central");
		
	}

	@Override
	void adicionar() {
		System.out.println("Adicionando Banco Central");
	}
}

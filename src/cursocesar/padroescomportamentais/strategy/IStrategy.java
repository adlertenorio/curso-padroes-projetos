package cursocesar.padroescomportamentais.strategy;

/**
 * @author Adler Ten�rio
 */
public interface IStrategy {

	public void executar();
}

package cursocesar.padroescomportamentais.strategy;
/**
 * @author Adler Ten�rio
 */
public abstract class AbstractRegistrarStrategy implements IStrategy {
	
	@Override
	public void executar() {
		criarCadastro();
		fazerAlgo();
		adicionar();
	}

	abstract void criarCadastro(); 
	abstract void fazerAlgo() ;
	abstract void adicionar();
}

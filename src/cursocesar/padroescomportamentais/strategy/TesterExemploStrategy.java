package cursocesar.padroescomportamentais.strategy;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Adler Ten�rio
 */

public class TesterExemploStrategy {

	public static void main(String[] args) {

		List<IStrategy> estrategias = new ArrayList<IStrategy>();

		IStrategy bancoCentral = new BancoCentral();
		IStrategy bancoCentralInterior = new BancoCentralInterior();

		estrategias.add(bancoCentral);
		estrategias.add(bancoCentralInterior);

		for (IStrategy estrategia : estrategias) {
			estrategia.executar();
			System.out.println("###########################");
		}
	}
}

package cursocesar.padroescomportamentais.strategy;

/**
 * @author Adler Ten�rio
 */
public abstract class AbstractRegistrarStrategyInterior implements IStrategy {
	
	@Override
	public void executar() {
		criarCadastro();
		adicionar();
		fazerAlgo();
		
	}

	abstract void criarCadastro(); 
	abstract void fazerAlgo() ;
	abstract void adicionar();
}
